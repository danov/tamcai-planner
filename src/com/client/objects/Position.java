package com.client.objects;

import java.math.BigInteger;
import java.util.Random;

import com.search.util.ILongHashCode;

public class Position implements ILongHashCode {

	public int x;
	public int y;
	private static long longPrime;
	private static long longBase;
	private static int prime = 0;
	private static int base = 0;

	public Position(int x, int y) {
		if (prime == 0) {
			BigInteger generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			prime = generator.intValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			base = generator.intValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			longPrime = generator.longValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			longBase = generator.longValue();
		}

		this.x = x;
		this.y = y;
	}

	public Position(Position p) {
		this(p.x, p.y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;

		if (getClass() != obj.getClass()) return false;
		Position other = (Position) obj;

		if (x != other.x) return false;
		if (y != other.y) return false;

		return true;
	}

	@Override
	public String toString() {
		return ("Position {" + x + ", " + y + "}");
	}

	@Override
	public Object clone() {
		return new Position(x, y);
	}

	@Override
	public int hashCode() {
		int result = base;
		result = prime * result + x;
		result = prime * result + y;
		result = prime * result + x + y;
		return result;
	}

	@Override
	public long longHashCode() {
		long result = longBase;
		result = longPrime * result + x;
		result = longPrime * result + y;
		result = longPrime * result + x + y;
		return result;
	}

}
