package com.client.objects;

import java.math.BigInteger;
import java.util.Random;

import com.search.util.ILongHashCode;

public class Goal extends StateObject {

	public int masterID;
	private static long longBase = 0;
	private static long longPrime = 0;
	private static int prime = 0;
	private static int base = 0;

	public Goal(int masterID, Character id, String color, int x, int y) {
		super(id, color, x, y);
		if (prime == 0) {
			BigInteger generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			prime = generator.intValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			base = generator.intValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			longPrime = generator.longValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			longBase = generator.longValue();
		}
		this.masterID = masterID;
	}

	@Override
	public boolean equals(Object o) {
		if (o != null && o instanceof Goal) {
			Goal temp = (Goal) o;
			return pos.equals(temp.pos) && id.equals(temp.id);
		} else return false;
	}

	@Override
	public Object clone() {
		return new Goal(masterID, id, color, pos.x, pos.y);
	}

	@Override
	public String toString() {
		return ("Goal {" + id + ", " + color + ", " + pos + "}");
	}

	@Override
	public int hashCode() {
		int result = base;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + masterID;
		result = prime * result + ((pos == null) ? 0 : pos.hashCode());
		return result;
	}

	@Override
	public long longHashCode() {
		long result = longBase;
		result = longPrime * result + ((id == null) ? 0 : id.hashCode());
		result = longPrime * result + masterID;
		result = longPrime * result + ((pos == null) ? 0 : pos.longHashCode());
		return result;
	}
}
