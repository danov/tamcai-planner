package com.client.objects;

import com.search.util.ILongHashCode;

public abstract class StateObject implements ILongHashCode {

	public Character id;
	public String color;
	public Position pos;

	public StateObject(Character id, String color, int x, int y) {
		this.id = new Character(id);
		this.color = new String(color);
		this.pos = new Position(x, y);
	}

	@Override
	public abstract int hashCode();

	@Override
	public abstract long longHashCode();

	@Override
	public abstract boolean equals(Object obj);

	@Override
	public abstract Object clone();
}
