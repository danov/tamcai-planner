package com.client.objects;

import java.math.BigInteger;
import java.util.Random;

import com.search.util.ILongHashCode;

public class Agent extends StateObject implements Comparable<Agent> {

	private static int prime = 0;
	private static int base = 0;

	private static long longPrime = 0;
	private static long longBase = 0;

	public Agent(Character id, String color, int x, int y) {
		super(id, color, x, y);
		if (prime == 0) {
			BigInteger generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			prime = generator.intValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			base = generator.intValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			longPrime = generator.longValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			longBase = generator.longValue();
		}
	}

	public Agent(Agent agent) {
		this(agent.id, agent.color, agent.pos.x, agent.pos.y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;

		if (getClass() != obj.getClass()) return false;

		Agent other = (Agent) obj;
		if (color == null) {
			if (other.color != null) return false;
		} else if (!color.equals(other.color)) return false;

		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;

		if (pos == null) {
			if (other.pos != null) return false;
		} else if (!pos.equals(other.pos)) return false;
		return true;
	}

	@Override
	public Object clone() {
		return new Agent(id, color, pos.x, pos.y);
	}

	@Override
	public String toString() {
		return ("Agent {" + id + ", " + color + ", " + pos + "}");
	}

	@Override
	public int hashCode() {
		int result = base;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + id;
		result = prime * result + ((pos == null) ? 0 : pos.hashCode());
		return result;
	}

	@Override
	public long longHashCode() {
		long result = longBase;
		result = longPrime * result + ((color == null) ? 0 : color.hashCode());
		result = longPrime * result + id;
		result = longPrime * result + ((pos == null) ? 0 : pos.longHashCode());
		return result;
	}

	@Override
	public int compareTo(Agent o) {
		return this.id - o.id;
	}
}
