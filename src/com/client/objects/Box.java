package com.client.objects;

public class Box extends Goal {

	public Box(int masterID, Character id, String color, int x, int y) {
		super(masterID, id, color, x, y);
	}

	public Box(Box box) {
		this(box.masterID, box.id, box.color, box.pos.x, box.pos.y);
	}

	@Override
	public Object clone() {
		return new Box(this);
	}

	@Override
	public String toString() {
		return ("Box {" + id + ", " + color + ", " + pos + "}");
	}

}
