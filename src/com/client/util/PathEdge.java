package com.client.util;

import com.client.objects.StateObject;
import com.client.problem.Rigids;

public class PathEdge implements Comparable<PathEdge> {

	public StateObject node1;
	public StateObject node2;

	public PathEdge(StateObject p1, StateObject p2) {
		this.node1 = p1;
		this.node2 = p2;
	}

	public int distance() {
		return Rigids.distance(node1.pos, node2.pos);
	}

	@Override
	public int compareTo(PathEdge o) {
		return distance() - o.distance();
	}

	@Override
	public boolean equals(Object o) {
		PathEdge obj = (PathEdge) o;
		return node1.equals(obj.node1) && node2.equals(obj.node2);
	}
	
	public boolean reversed(Object o) {
		PathEdge obj = (PathEdge) o;
		return node1.equals(obj.node2) && node2.equals(obj.node1);
	}

	@Override
	public String toString() {
		return "PathEdge {" + node1 + ", " + node2 + "}";
	}

	@Override
	public int hashCode() {
		final int prime = 337;
		int result = 613;
		result = prime * result + ((node1 == null) ? 0 : node1.hashCode()) + ((node2 == null) ? 0 : node2.hashCode());
		return result;
	}
}
