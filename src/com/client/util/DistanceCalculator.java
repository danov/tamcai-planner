package com.client.util;

import java.util.ArrayDeque;
// TODO: At the end to be rewritten with Johnson's algorithm, because it has better time complexity than this one. Also we need the small closed spaces to be iterated also.
public class DistanceCalculator {
	//assuming square 2-dimensional array
	private static boolean [][] walls;
	private static Place[][] places;
	private static int width, height;
	private static int[][][][] distance;
	private static ArrayDeque<Place> frontier;
	public static boolean test = false;
	public static int testX = 0;
	public static int testY = 0;
	private static int explored;
	/**
	 * Calculates distances between all places reachable from given starting coordinates.
	 * For unreachable places distances are not calculated and are all equal to Integer.MAX_VALUE
	 * Method is synchronized just in case because it stores a lot of data in static attributes.
	 * @param walls - array of walls where true means that the wall is present.
	 * @param startX - x coordinate of the place from which to start searching for all reachable places.
	 * @param startY - y coordinate of the place from which to start searching for all reachable places.
	 * @return array of distances between each point.
	 */
	public static synchronized int[][][][] calculateDistances(boolean[][] walls, int startX, int startY) {
		DistanceCalculator.walls = walls;
		width = walls.length;
		height = walls[0].length;
		places = new Place[width][height];
		frontier = new ArrayDeque<Place>();
		distance = new int[width][height][width][height];// from [x1][y1] to [x2][y2]
		explored = 0;// reset counter of explored states
		BFSreachablePlaces(startX, startY);
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				for(int x = 0; x < width; x++) {
					for(int y = 0; y < height; y++) {
						distance[i][j][x][y] = Integer.MAX_VALUE;
					}
				}
			}
		}
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				if(places[i][j] != null)// if place is null it is either wall or unreachable
					BFSdistancesFromPoint(i, j);
			}
		}
		if(test) {
			for(int j = 0; j < walls[0].length; j++) {
				for(int i = 0; i < walls.length; i++) {
					int dst = distance[testX][testY][i][j];
					if(dst == Integer.MAX_VALUE)
						dst = -1;
					System.err.print(String.format("%1$02d ", dst));
				}
				System.err.println();
			}
			System.err.println("Nodes explored: "+explored);
		}
		return distance;
	}
	
	/**
	 * Finds all reachable places from the given initial coordinates.
	 * "places" array is populated only with places that are reachable and that are not walls.
	 * Modifies static "places" array.
	 * @param i - x coordinate of place for which all reachable places are to be found.
	 * @param j - y coordinate of place for which all reachable places are to be found.
	 */
	private static void BFSreachablePlaces(int i, int j) {
		Place place = new Place(i, j);
		Place next;
		frontier.offerLast(place);
		while(!frontier.isEmpty()) {
			place = frontier.poll();
			for(int c = 0; c < 4; c++) {// for all neighbors of place
				next = getNeighbor(place.x, place.y, c);
				if(next != null && !walls[next.x][next.y] && places[next.x][next.y] == null) {
					// gets here only if neighbor exists and is not yet processed
					places[next.x][next.y] = next;
					frontier.offerLast(next);
				}
			}
		}
	}

	/**
	 * Calculates all distances from specified coordinates.
	 * This BFS algorithm ends its calculation only when frontier is empty.
	 * Modifies static distance array.
	 * @param i - x coordinate of place for which distances are to be found.
	 * @param j - y coordinate of place for which distances are to be found.
	 */
	private static void BFSdistancesFromPoint(int i, int j) {
		Place place;
		Place next;
		// clear status of each place
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				if(places[x][y] != null)
					places[x][y].checked = false;
			}
		}
		distance[i][j][i][j] = 0;// start node distance = 0
		places[i][j].checked = true;// marking initial place as already added to frontier
		frontier.offerLast(places[i][j]);
		while(!frontier.isEmpty()) {
			place = frontier.pollFirst();
			explored++;
			for(int c = 0; c < 4; c++) {// for all neighbors of place
				next = getNeighbor(place, c);
				if(next != null && !next.checked) {
					next.checked = true;// guarantees that this place won't be added to frontier again
					distance[i][j][next.x][next.y] = distance[i][j][place.x][place.y] + 1;
					frontier.offerLast(next);
				}
			}
		}
	}
	
	/**
	 * Returns a Place object of index'th neighbor or null if such place does not exist.
	 * Here non-existing place also means a wall or (this was determined during 
	 * BFSreachablePlaces run) unreachable place (should never happen).
	 * @param place - place for which neighbor is to be returned.
	 * @param index - index determining which neighbor is to be returned.
	 * @return Place object with neighbor.
	 */
	private static Place getNeighbor(Place place, int index) {
		int x = place.x, y = place.y;
		switch(index) {
		case 0:
			if(x-1 >= 0)
				return places[x-1][y];
		case 1:
			if(y+1 < height)
				return places[x][y+1];
		case 2:
			if(x+1 < width)
				return places[x+1][y];
		case 3:
			if(y-1 >= 0)
				return places[x][y-1];
		}
		return null;
	}
	
	/**
	 * Creates and returns a Place object of index'th neighbor or null if neighbor's
	 * coordinates would not fit into places array.
	 * @param x - x coordinate of place for which neighbor is to be returned.
	 * @param y - y coordinate of place for which neighbor is to be returned.
	 * @param index - index determining which neighbor is to be returned.
	 * @return Place object with neighbor.
	 */
	private static Place getNeighbor(int x, int y, int index) {
		switch(index) {
		case 0:
			if(x-1 >= 0)
				return new Place(x-1, y);
		case 1:
			if(y+1 < height)
				return new Place(x, y+1);
		case 2:
			if(x+1 < width)
				return new Place(x+1, y);
		case 3:
			if(y-1 >= 0)
				return new Place(x, y-1);
		}
		return null;
	}
}

class Place {
	public int x;
	public int y;
	boolean checked;
	public Place(int x, int y) {
		this.x = x;
		this.y = y;
		checked = false;
	}
}
