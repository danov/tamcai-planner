package com.client.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Union-Find Data structure.
 * By calling the makeUnionFind(S) a one element set is made for every object s contained in S
 * Objects are stored within Records which are put in trees. A Set can be recognised
 * by a toplevel record (with a parent pointing to null)
 * 
 * This not a general "Set handling" class, but made for being used with for example
 * Kruskal's Algortihm.
 * See http://en.wikipedia.org/wiki/Disjoint-set_data_structure for more info!
 * -------------------------------------
 * Implements makeUnionFind(S) in O(n), Union in O(log n), or O(1) if you a record representing a set,
 * and find(u) called n times yieald an amortized running time of per calling of O(a(n)) ~= O(1).
 * 
 **/

public class UnionFind<E> {

	/* Record class. Defines a set, or a record within a set */
	private class Record<T> {

		private int size;
		private T name;
		private Record<T> parent = null;

		public Record(T name) {
			this.name = name;
			size = 1;
		}

		public void setParent(Record<T> parent) {
			this.parent = parent;
		}

		public T getName() {
			return name;
		}

		private int getSize() {
			return size;
		}

		private void addToSize(int add) {
			size += add;
		}

		public Record<T> getParent() {
			return parent;
		}

		@SuppressWarnings("unchecked")
		public boolean equals(Object obj) {
			if (obj == null || getClass() != obj.getClass()) {
				return false;
			} else {
				Record<T> o = (Record<T>) obj;
				return name.equals(o.getName());
			}
		}
	}

	/* data member - ArrayList containing all the records */
	private ArrayList<Record<E>> records = new ArrayList<Record<E>>();
	private HashMap<E, Record<E>> keymap = new HashMap<E, Record<E>>();

	/* Initalizes all sets, one for every element in list set */
	public void makeUnionFind(List<E> set) {
		for (E it : set)
			makeSet(it);
	}

	public void makeSet(E el) {
		Record<E> rec = new Record<E>(el);
		keymap.put(el, rec);
		records.add(rec);
	}

	public Collection<E> getElements() {
		return keymap.keySet();
	}

	/* "Unionizes two sets */
	public void union(E x, E y) {
		Record<E> rec1 = keymap.get(x);
		Record<E> rec2 = keymap.get(y);

		Record<E> xroot = find(rec1);
		Record<E> yroot = find(rec2);

		if (xroot.getSize() > yroot.getSize()) {
			yroot.setParent(xroot);
			xroot.addToSize(yroot.getSize());
		} else {
			xroot.setParent(yroot);
			yroot.addToSize(xroot.getSize());
		}
	}

	/* Given a records returns the top-record that represents the set 
	 * containing that record. Re-links the given record to the top-record (Path compression,
	 * the key to gain the amortized running time).
	 **/
	private Record<E> find(Record<E> rec) {
		if (rec.getParent() == null) return rec;
		else {
			rec.setParent(find(rec.getParent()));
			return rec.getParent();
		}
	}
	
	public E find(E el) {
		Record<E> rec = keymap.get(el);
		return find(rec).getName();
	}
	
	public int getSetSize(E el) {
		Record<E> rec = keymap.get(el);
		return find(rec).getSize();
	}

	/* Checks if to records are in the same set. */
	public boolean sameSet(E r1, E r2) {
		Record<E> rec1 = keymap.get(r1);
		Record<E> rec2 = keymap.get(r2);

		return find(rec1).equals(find(rec2));
	}

}
