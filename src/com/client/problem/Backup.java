package com.client.problem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;

import com.client.objects.Position;

public class Backup {
	public ArrayList<WorldState> previousState;
	public HashMap<PlanningAgent, Integer> cutOffs;
	public ArrayList<LinkedHashSet<Position>> previousReserved;
}
