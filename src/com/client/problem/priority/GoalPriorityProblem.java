package com.client.problem.priority;

import java.util.LinkedList;

import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.objects.Position;
import com.client.problem.Rigids;
import com.search.AbstractSearchProblem;
import com.search.IProblemAction;
import com.search.IProblemState;
import com.search.SearchNode;

public class GoalPriorityProblem extends AbstractSearchProblem {

	private Goal goal;
	private Box box;

	public class GoalPriorityAction implements IProblemAction {

		public PositionState target;
		public int stepCost = 1;

	}

	public class PositionState implements IProblemState {

		public Position pos;
		public int goalsCrossed;
		private static final int prime = 0;
		private static final int base = 0;

		public PositionState(Position pos, int goalsCrossed) {
			this.pos = pos;
			this.goalsCrossed = goalsCrossed;
		}

		@Override
		public boolean equals(Object o) {
			PositionState temp = (PositionState) o;
			if (o != null && o.getClass().equals(getClass())) return pos.equals(temp.pos);
			else return false;
		}

		@Override
		public int hashCode() {
			int result = base;
			result = prime * result + ((pos == null) ? 0 : pos.hashCode());
			return result;
		}

		@Override
		public long longHashCode() {
			return hashCode();
		}
	}

	public GoalPriorityProblem(Goal goal, Box box) {
		this.goal = goal;
		this.box = box;
	}

	@Override
	public boolean goalTest(SearchNode node) {
		PositionState pState = (PositionState) node.state;
		return pState.pos.equals(box.pos);
	}

	@Override
	public LinkedList<IProblemAction> buildSolution(SearchNode node) {
		LinkedList<IProblemAction> actions = new LinkedList<IProblemAction>();
		actions.add(node.action);
		return actions;
	}

	@Override
	public SearchNode initialState() {
		SearchNode nsn = new SearchNode();
		nsn.parent = null;
		nsn.pathCost = 0;
		nsn.state = new PositionState(goal.pos, 0);
		nsn.action = null;
		return nsn;
	}

	@Override
	public LinkedList<IProblemAction> getActions(SearchNode node) {
		LinkedList<IProblemAction> result = new LinkedList<IProblemAction>();
		PositionState state = (PositionState) node.state;
		for (Position p : Rigids.nearbyPositions(state.pos).values()) {
			if (!Rigids.isWall(p)) {
				GoalPriorityAction gpAction = new GoalPriorityAction();
				gpAction.target = new PositionState(p, state.goalsCrossed);
				Goal g = Rigids.getGoal(p);
				if (g != null && !g.equals(goal) && !g.equals(box)) {
					gpAction.stepCost += 3;
					gpAction.target.goalsCrossed++;
				}
				result.add(gpAction);
			}
		}
		return result;
	}

	@Override
	public SearchNode childNode(SearchNode node, IProblemAction action) {
		GoalPriorityAction gpAction = (GoalPriorityAction) action;
		SearchNode nsn = new SearchNode();
		nsn.parent = node;
		nsn.pathCost = node.pathCost + gpAction.stepCost;
		nsn.heuristicCost = Rigids.distance(gpAction.target.pos, box.pos);
		nsn.state = gpAction.target;
		nsn.action = gpAction;
		return nsn;
	}

	@Override
	public String toString() {
		return "GoalPriorityProblem {" + goal + ", " + box + "}";
	}

}
