package com.client.problem.priority;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.client.problem.priority.GoalPriorityProblem.GoalPriorityAction;
import com.client.util.Utils;
import com.search.IProblemAction;
import com.search.Search;

public class GoalPriorities {

	@SuppressWarnings("unchecked")
	public static LinkedHashMap<Goal, Integer> getGoalsPriorities(WorldState initial) {
		LinkedHashMap<Goal, Integer> priorities = new LinkedHashMap<Goal, Integer>();

		for (Goal g : Rigids.getGoals()) {
			int min = Integer.MAX_VALUE;
			for (Box b : initial.getBoxes(g.id)) {
				if (!g.equals(b)) {
					GoalPriorityProblem problem = new GoalPriorityProblem(g, b);
					LinkedList<IProblemAction> plan = Search.aStar(problem);
					if (plan != null) {
						GoalPriorityAction gpAction = (GoalPriorityAction) plan.getLast();
						int goalsCrossed = gpAction.target.goalsCrossed;
						min = goalsCrossed < min ? goalsCrossed : min;
					}
				}
			}
			priorities.put(g, min);
		}

		priorities = Utils.sortByValue(priorities);
		System.err.println("Priorities: " + priorities);
		return priorities;
	}
}
