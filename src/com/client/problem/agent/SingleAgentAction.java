package com.client.problem.agent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.objects.Position;
import com.client.objects.StateObject;
import com.client.problem.AgentPriorities;
import com.client.problem.BigActionType;
import com.client.problem.Direction;
import com.client.problem.Request;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.client.problem.goal.SingleGoalProblem;
import com.client.problem.goal.action.PullAction;
import com.client.problem.goal.action.PushAction;
import com.client.problem.goal.action.SingleGoalAction;
import com.client.problem.heuristics.NearestFreeProblem;
import com.search.IProblemAction;
import com.search.Search;

public class SingleAgentAction implements IProblemAction {

	private SingleGoalProblem problem;
	public LinkedList<IProblemAction> plan;
	public boolean wrongPriority = false;

	public WorldState target;

	public int stepCost;
	public int heuristicsCost;

	public Agent agent;
	public Box box;
	public Goal goal;
	public BigActionType type;
	public Direction dir;
	private String specs;
	public Request request;
	public LinkedHashSet<Position> path = new LinkedHashSet<Position>();
	public WorldState initial;

	public SingleAgentAction(WorldState initial, BigActionType type, Direction dir, Agent a, Box b, Goal g) {
		specs = a.id + ", " + b.id + ", " + g.id;
		this.type = type;
		this.dir = dir;
		this.initial = initial;
		problem = new SingleGoalProblem(initial, type, dir, a, b, g);
		problem.priority = AgentPriorities.getPriority(a);
		agent = a;
		box = b;
		solveProblem();
	}

	public SingleAgentAction(WorldState initial, BigActionType type, Agent a, Box b) {
		specs = a.id + ", " + b.id;
		this.type = type;
		this.initial = initial;
		problem = new SingleGoalProblem(initial, type, dir, a, b);
		problem.priority = AgentPriorities.getPriority(a);
		agent = a;
		box = b;
		solveProblem();
	}
	//without direction
	public SingleAgentAction(WorldState initial, BigActionType type, Agent a, Box b, Goal g) {
		specs = a.id + ", " + b.id + ", " + g.id;
		this.type = type;
		this.initial = initial;
		problem = new SingleGoalProblem(initial, type, dir, a, b, g);
		problem.priority = AgentPriorities.getPriority(a);
		box = b;
		goal = g;
		agent = a;
		solveProblem();
	}

	public SingleAgentAction(WorldState initial, BigActionType type, Direction dir, Agent a, Box b1, Box b2) {
		specs = a.id + ", " + b1.id + ", " + b2.id;
		this.type = type;
		this.dir = dir;
		this.initial = initial;
		problem = new SingleGoalProblem(initial, type, dir, a, b1, b2);
		problem.priority = AgentPriorities.getPriority(a);
		agent = a;
		solveProblem();
	}
	
	public SingleAgentAction(WorldState initial, BigActionType type, Agent a, LinkedList<StateObject> objectsToMove, LinkedHashSet<Position> pathToClear, int priority) {
		specs = a.id + ", " + objectsToMove.toString();
		this.type = type;
		this.initial = initial;
		agent = a;
		LinkedHashMap<StateObject, Position> objFreePlace = findFreePlaces(initial, objectsToMove, pathToClear);
		problem = new SingleGoalProblem(initial, type, a, objFreePlace, pathToClear);
		problem.priority = priority;
		solveProblem();
	}

	protected void solveProblem() {
		if (problem == null) return;
		problem.establishHeuristics();
		//here check if the action doesn't invalidate goal with higher priority
		//might cause problems in MultiAgent
		//TODO: will not be needed when MST is integrated with priorities
		//COMMENTED OUT for now, it should work even without it
//		if(goal != null && Rigids.nextPriorityLevel(target) > Rigids.getPriority(goal)) {
//			plan = null;
//			wrongPriority = true;
//		}
//		else
			plan = Search.aStar(problem);
		//request = problem.request//here setting the request
		target = (WorldState) problem.getFinalState(plan).clone();
		if (plan != null) {// && !plan.isEmpty()) {
			stepCost = plan.size();
			path.add(initial.getAgent(agent).pos);
			for(IProblemAction action : plan) {
				SingleGoalAction sgAction = (SingleGoalAction) action;
				path.add(sgAction.pos);
				if(sgAction instanceof PushAction) {
					path.add(((PushAction)sgAction).box.pos);
				}
				else if(sgAction instanceof PullAction) {
					path.add(((PullAction)sgAction).box.pos);
				}
			}
			//here finding freePlaces for conflicting objects
			if(problem.conflictingAgent != null || problem.conflictingBoxes != null) {
				LinkedList<StateObject> objectsOnWay = new LinkedList<StateObject>();
				if(problem.conflictingAgent != null) 
					objectsOnWay.add(problem.conflictingAgent);
				if(problem.conflictingBoxes != null)
					objectsOnWay.addAll(problem.conflictingBoxes);
				//build request
				request = new Request(objectsOnWay, path, problem.priority, objectsOnWay.get(0).color);
				if(problem.conflictingAgent != null)
					request.agentInvolved = problem.conflictingAgent;
				//nullify plan (not to follow it)
				plan = null;
			}
		}
		else stepCost = Integer.MAX_VALUE / 2;
	}
	
	protected LinkedHashMap<StateObject, Position> findFreePlaces(WorldState initial, LinkedList<StateObject> objectsToMove, LinkedHashSet<Position> pathToClear) {
		LinkedHashMap<StateObject, Position> objFreePlace = new LinkedHashMap<StateObject, Position>();
		LinkedList<StateObject> objects = new LinkedList<StateObject>(objectsToMove);
		StateObject obj;
		while(!objects.isEmpty()) {
			obj = objects.remove();
			NearestFreeProblem problem = new NearestFreeProblem(initial, pathToClear, obj, objFreePlace, objectsToMove, null, null);
			//objFreePlace and objectsToMove are modified during search DO NOT REFACTOR IT!!!!!
			Search.breadthFirstSearch(problem);
		}
		return objFreePlace;
	}

	@Override
	public boolean equals(Object o) {
		if (o != null && o.getClass().equals(getClass())) {
			return problem.equals(o) && stepCost == ((SingleAgentAction) o).stepCost;
		} else return false;
	}

	@Override
	public int hashCode() {
		return problem.hashCode() << 16 | stepCost;
	}

	@Override
	public String toString() {
		return ("Action {" + type + ", " + dir + ", " + specs + ", " + stepCost + ", " + heuristicsCost + "}");
	}
}
