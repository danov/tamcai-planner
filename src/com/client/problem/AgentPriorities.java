package com.client.problem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import org.teneighty.heap.Comparators;

import com.client.objects.Agent;

public class AgentPriorities {
	public static int HIGHEST_PRIORITY;
	private static HashMap<PlanningAgent,Integer> agent_priority = new HashMap<PlanningAgent,Integer>();
	private static HashMap<Integer,PlanningAgent> priority_agent = new HashMap<Integer,PlanningAgent>();
	private static HashMap<Character,PlanningAgent> agent_planning = new HashMap<Character, PlanningAgent>();
	
	public static void initialize(HashMap<PlanningAgent, Integer> initialPathLength) {
		PlanningAgent agent;
		//achieving total ordering
		HIGHEST_PRIORITY = initialPathLength.size() - 1;
		for(int i = HIGHEST_PRIORITY; i >= 0; i--) {
			agent = getLowest(initialPathLength);
			AgentPriorities.agent_priority.put(agent, i);
			AgentPriorities.priority_agent.put(i, agent);
			AgentPriorities.agent_planning.put(agent.agent.id, agent);
			initialPathLength.remove(agent);
		}
	}
	
	private static PlanningAgent getLowest(HashMap<PlanningAgent, Integer> initialPathLength) {
		PlanningAgent lowest = null;
		int min = Integer.MAX_VALUE;
		for(Entry<PlanningAgent, Integer> e : initialPathLength.entrySet()) {
			if(e.getValue() < min) {
				min = e.getValue();
				lowest = e.getKey();
			}
		}
		return lowest;
	}
	
	public static PlanningAgent getHighestAgent() {
		return priority_agent.get(HIGHEST_PRIORITY);
	}
	
	public static int getPriority(PlanningAgent agent) {
		return agent_priority.get(agent);
	}
	
	public static int getPriority(Agent agent) {
		PlanningAgent pAgent = agent_planning.get(agent.id);
		return agent_priority.get(pAgent);
	}
	
	public static PlanningAgent getAgent(int priority) {
		return priority_agent.get(priority);
	}
	
	public static PlanningAgent getAgent(char id) {
		return agent_planning.get(id);
	}
	
	public static PlanningAgent getHigherAgent(int priority) {
		if(priority == HIGHEST_PRIORITY)
			return priority_agent.get(HIGHEST_PRIORITY);
		else {
			priority++;
			return priority_agent.get(priority);
		}
	}
	
	//switching priorities works like bubbles changing position (all priorities remain distinct)
	public static void setPriority(PlanningAgent agent, int newPriority) {
		int oldPriority = getPriority(agent);
		if(oldPriority > newPriority) {
			for(Entry<PlanningAgent, Integer> e : agent_priority.entrySet()) {
				if(e.getValue() < oldPriority && e.getValue() >= newPriority) {
					priority_agent.put(e.getValue()+1, e.getKey());
					e.setValue(e.getValue()+1);
				}
			}
		}
		else if(oldPriority < newPriority) {
			for(Entry<PlanningAgent, Integer> e : agent_priority.entrySet()) {
				if(e.getValue() > oldPriority && e.getValue() <= newPriority) {
					priority_agent.put(e.getValue()-1, e.getKey());
					e.setValue(e.getValue()-1);
				}
			}
		}
		agent_priority.put(agent, newPriority);
		priority_agent.put(newPriority, agent);
	}
	
	public static Collection<PlanningAgent> getPlanningAgents() {
		return agent_priority.keySet();
	}
}
