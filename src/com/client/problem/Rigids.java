package com.client.problem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.objects.Position;
import com.client.problem.goal.action.NoOpAction;
import com.client.problem.goal.action.SingleGoalAction;
import com.client.util.Utils;
import com.search.IProblemAction;

public class Rigids {

	public static boolean[][] walls;
	public static int[][][][] distances;

	protected static Map<Character, ArrayList<Goal>> goalsByID = new HashMap<Character, ArrayList<Goal>>();
	protected static Map<String, ArrayList<Goal>> goalsByColor = new HashMap<String, ArrayList<Goal>>();
	protected static Map<Position, Goal> goalsByPos = new HashMap<Position, Goal>();
	public static Map<Goal, Integer> goalPriorities = new HashMap<Goal, Integer>();
	private static int maxPriority;

	public static ArrayList<WorldState> round_states = new ArrayList<WorldState>();
	public static HashMap<PlanningAgent, LinkedList<IProblemAction>> agent_plan = new HashMap<PlanningAgent, LinkedList<IProblemAction>>();
	//this one will contain reserved places on which other agents cannot leave boxes for specific rounds
	//so when a set of places is present at a given round (index) then until that time those places are reserved
	public static ArrayList<LinkedHashSet<Position>> reserved_places = new ArrayList<LinkedHashSet<Position>>();

	public static void addGoal(Goal g) {
		goalsByPos.put(new Position(g.pos), g);

		ArrayList<Goal> goalsById = (ArrayList<Goal>) Rigids.getGoals(g.id);
		if (goalsById == null) {
			goalsById = new ArrayList<Goal>();
			Rigids.goalsByID.put(g.id, goalsById);
		}
		goalsById.add(g);

		ArrayList<Goal> goalsByColor = (ArrayList<Goal>) Rigids.getGoals(g.color);
		if (goalsByColor == null) {
			goalsByColor = new ArrayList<Goal>();
			Rigids.goalsByColor.put(g.color, goalsByColor);
		}
		goalsByColor.add(g);
	}

	public static Goal getGoal(Position pos) {
		return goalsByPos.get(pos);
	}

	public static Collection<Character> getGoalIDs() {
		return goalsByID.keySet();
	}

	public static Collection<Goal> getGoals() {
		return goalsByPos.values();
	}

	public static ArrayList<Goal> getGoals(Character id) {
		return goalsByID.get(id);
	}

	public static ArrayList<Goal> getGoals(String color) {
		return goalsByColor.get(color);
	}

	public static boolean isInBoundries(Position p) {
		return p.x >= 0 && p.x < Rigids.walls.length && p.y >= 0 && p.y < Rigids.walls[0].length;
	}

	public static boolean isWall(Position p) {
		return Rigids.walls[p.x][p.y];
	}

	public static Direction getDirection(Position p1, Position p2) {
		if (p1.y == p2.y) {
			if (p1.x > p2.x) {
				return Direction.W;
			} else {
				return Direction.E;
			}
		} else if (p1.y > p2.y) {
			return Direction.N;
		} else {
			return Direction.S;
		}
	}

	public static HashMap<Direction, Position> nearbyPositions(Position pos) {
		HashMap<Direction, Position> result = new HashMap<Direction, Position>();

		Position N = new Position(pos.x, pos.y - 1);
		Position S = new Position(pos.x, pos.y + 1);
		Position W = new Position(pos.x - 1, pos.y);
		Position E = new Position(pos.x + 1, pos.y);

		if (isInBoundries(N)) result.put(Direction.N, N);
		if (isInBoundries(S)) result.put(Direction.S, S);
		if (isInBoundries(W)) result.put(Direction.W, W);
		if (isInBoundries(E)) result.put(Direction.E, E);

		return result;
	}

	public static int distance(Position p1, Position p2) {
		return Rigids.distances[p1.x][p1.y][p2.x][p2.y];
	}

	public static void setGoalPriorities(Map<Goal, Integer> goalPriorities) {
		Rigids.goalPriorities = goalPriorities;
		int max = 0;
		for (Integer i : goalPriorities.values())
			if (i > max) max = i;
		maxPriority = max;
	}

	public static boolean isNextPriorityLevel(WorldState ws, int level) {
		if (level == maxPriority) return true;
		int higherLevel = level + 1;
		for (Entry<Goal, Integer> e : goalPriorities.entrySet())
			if (e.getValue() == higherLevel && !ws.isAchieved(e.getKey())) return false;
		//maxPriority = level;// assuming that when the goal is achieved it remains that way
		return true;
	}

	public static int nextPriorityLevel(WorldState ws) {
		int level = 0;
		for (Entry<Goal, Integer> e : goalPriorities.entrySet())
			if (e.getValue() > level && !ws.isAchieved(e.getKey())) level = e.getValue();
		return level;
	}

	public static ArrayList<Box> getBoxesOnGoalsAboveLevel(WorldState ws, int level) {
		ArrayList<Box> boxes = new ArrayList<Box>();
		for (Entry<Goal, Integer> e : goalPriorities.entrySet())
			if (e.getValue() > level) {
				if (!ws.isAchieved(e.getKey())) System.err.println("Error in Rigids.getOnGoalAboveLevel");
				boxes.add(ws.getBox(e.getKey().pos));
			}
		return boxes;
	}

	public static int getPriority(Goal g) {
		return goalPriorities.get(g);
	}

	public static Map<Goal, Integer> getPriorities() {
		return Utils.sortByValue(goalPriorities);
	}

	public static int getMaxRound() {
		return round_states.size() - 1;
	}

	public static WorldState getWorldState(int round) {
		if (round > round_states.size()) System.err.println("Error in Rigids.getWorldState");
		if (round == round_states.size()) {
			round_states.add((WorldState) round_states.get(round - 1).clone());
			round_states.get(round).round = round;
		}
		return round_states.get(round);
	}

	public static LinkedList<IProblemAction> getAgentPlan(PlanningAgent pAgent) {
		if (!Rigids.agent_plan.containsKey(pAgent)) Rigids.agent_plan.put(pAgent, new LinkedList<IProblemAction>());
		return agent_plan.get(pAgent);
	}

	public static LinkedHashSet<Position> getReservedPlaces(int round) {
		if (round >= reserved_places.size()) {
			for (int i = reserved_places.size(); i <= round; i++) {
				reserved_places.add(new LinkedHashSet<Position>());
			}
		}
		return reserved_places.get(round);
	}

	public static ArrayList<WorldState> getStateBackup() {
		ArrayList<WorldState> backup_round_states = new ArrayList<WorldState>();
		for (WorldState ws : round_states) {
			backup_round_states.add((WorldState) ws.clone());
		}
		return backup_round_states;
	}

	public static HashMap<PlanningAgent, Integer> getPlansBackup() {
		HashMap<PlanningAgent, Integer> plansSizes = new HashMap<PlanningAgent, Integer>();
		for (Entry<PlanningAgent, LinkedList<IProblemAction>> e : agent_plan.entrySet()) {
			plansSizes.put(e.getKey(), e.getValue().size());
		}
		return plansSizes;
	}

	public static void restorePlans(HashMap<PlanningAgent, Integer> cutOffs) {
		for (Entry<PlanningAgent, Integer> e : cutOffs.entrySet()) {
			LinkedList<IProblemAction> originalPlan = new LinkedList<IProblemAction>();
			//(LinkedList<IProblemAction>) agent_plan.get(e.getKey()).subList(0, e.getValue());
			for (int i = 0; i < e.getValue(); i++)
				originalPlan.add(agent_plan.get(e.getKey()).get(i));
			agent_plan.put(e.getKey(), originalPlan);
		}
	}

	public static ArrayList<LinkedHashSet<Position>> getReservedBackup() {
		ArrayList<LinkedHashSet<Position>> backup_reserved = new ArrayList<LinkedHashSet<Position>>();
		for (LinkedHashSet<Position> set : reserved_places) {
			backup_reserved.add(set);
		}
		return backup_reserved;
	}

	public static int resetWorldState() {
		int round = round_states.size();
		ArrayList<WorldState> newWs = new ArrayList<WorldState>();
		newWs.add(round_states.get(0));
		round_states = newWs;
		return round;
	}

	public static void restoreReserved(ArrayList<LinkedHashSet<Position>> oldReserved) {
		reserved_places = oldReserved;
	}

	public static void restoreState(ArrayList<WorldState> previousState) {
		round_states = previousState;
	}

	public static ArrayList<LinkedList<IProblemAction>> getOrderedPlans() {
		ArrayList<LinkedList<IProblemAction>> plans = new ArrayList<LinkedList<IProblemAction>>();
		int maxSize = 0;
		for (int i = 0; i <= 9; i++) {
			char id = String.valueOf(i).charAt(0);
			PlanningAgent pAgent = AgentPriorities.getAgent(id);
			if (agent_plan.containsKey(pAgent)) {
				LinkedList<IProblemAction> plan = agent_plan.get(pAgent);
				plans.add(plan);
				if (maxSize < plan.size()) maxSize = plan.size();
			}
		}
		for (LinkedList<IProblemAction> plan : plans) {
			for (int i = plan.size(); i < maxSize; i++) {
				SingleGoalAction sga = (SingleGoalAction) plan.get(i - 1);
				plan.add(new NoOpAction(sga.agent, sga.pos));
			}
		}
		return plans;
	}
}
