package com.client.problem;

public enum BigActionType {
	GOTO_BOX, CARRYTO_BOX, CARRYTO_GOAL, MOVE_AWAY;
}
