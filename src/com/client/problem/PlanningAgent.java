package com.client.problem;

import java.util.LinkedList;

import sun.misc.GC.LatencyRequest;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.problem.agent.SingleAgentAction;
import com.client.problem.goal.SingleGoalProblem;
import com.client.problem.heuristics.SingleAgentHeuristics;
import com.client.util.PathEdge;
import com.search.IProblemAction;



public class PlanningAgent {
	public Agent agent;
	private LinkedList<PathEdge> actions;
	public PlanningAgent(Agent agent) {
		this.agent = agent;
	}
	private SingleAgentAction lastAction;
	
	public SingleAgentAction planForRequest(Request request) {
		return new SingleAgentAction(Rigids.getWorldState(getLastRound()), BigActionType.MOVE_AWAY, agent, request.objectsToMove, request.pathToClear, request.priority);
	}
	
	public void establishActionsOrder() {
		//TODO: here the MST should be calculated
		//to use only if after failing to complete current action next action would be suggested
	}
	
	public SingleAgentAction getNextBestAction() {
		WorldState state = Rigids.getWorldState(getLastRound());
		PathEdge bestEdge = actions.getFirst();
		PathEdge nextEdge = actions.get(1);
		if(lastAction != null && lastAction.type == BigActionType.GOTO_BOX && Rigids.distance(state.getAgent(lastAction.agent).pos, state.getBox(lastAction.box).pos) == 1) {
			Agent a = (Agent) state.getAgent(lastAction.agent).clone();
			Box b = (Box) state.getBox(lastAction.box).clone();
			Goal g = (Goal) lastAction.goal;
			SingleAgentAction sga = new SingleAgentAction(state, BigActionType.CARRYTO_GOAL, a, b, g);
			lastAction = sga;
			return sga;
		}
		else {
			Agent a = (Agent) state.getAgent(agent).clone();
			Box b = (Box) state.getBox((Box)bestEdge.node2).clone();
			SingleAgentAction sga = new SingleAgentAction(state, BigActionType.GOTO_BOX, a, b, (Goal) nextEdge.node2);
			lastAction = sga;
			return sga;
		}
	}
	
	public boolean hasOwnNextAction() {
		//TODO: here agent should return whether its full plan is complete or not
		WorldState state = Rigids.getWorldState(getLastRound());
		actions = SingleAgentHeuristics.approximatedShortestPath(state, agent);
		if(actions.isEmpty())
			return false;
		else return true;
	}
	
	public void nextAction() {
		//TODO: change what getNextBestAction() returns to next best action
		//to use only if after failing to complete current action next action would be suggested
	}
	
	public int getLastRound() {
		if(Rigids.agent_plan.get(this) != null)
			return Rigids.agent_plan.get(this).size();
		else return 0;
	}
	
	@Override
	public int hashCode() {
		return agent.id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PlanningAgent)
			return agent.id.equals(((PlanningAgent) obj).agent.id);
		else return false;
	}
	
}
