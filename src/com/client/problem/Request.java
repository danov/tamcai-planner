package com.client.problem;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import com.client.objects.Agent;
import com.client.objects.Position;
import com.client.objects.StateObject;
import com.search.IProblemAction;

public class Request {
	public LinkedList<StateObject> objectsToMove;
	public LinkedHashSet<Position> pathToClear;
	public int priority;//set to priority of agent that calls request
	public ArrayList<LinkedList<IProblemAction>> responses = new ArrayList<LinkedList<IProblemAction>>();
	public String color;
	public Agent agentInvolved; // indicates whether there is an agent that has to move away
	
	public Request(LinkedList<StateObject> objectsToMove, LinkedHashSet<Position> pathToClear, int priority, String color) {
		this.objectsToMove = objectsToMove;
		this.pathToClear = pathToClear;
		this.priority = priority;
		this.color = color;
	}
	public void addResponse(LinkedList<IProblemAction> plan) {
		responses.add(plan);
	}
	
	public LinkedList<IProblemAction> getBestPlan() {
		LinkedList<IProblemAction> best = null;
		int min = Integer.MAX_VALUE;
		for(LinkedList<IProblemAction> plan : responses) {
			if(plan.size() < min) {
				min = plan.size();
				best = plan;
			}
		}
		return best;
	}
	
}
