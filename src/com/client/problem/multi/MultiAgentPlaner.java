package com.client.problem.multi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.client.objects.Agent;
import com.client.problem.AgentPriorities;
import com.client.problem.Backup;
import com.client.problem.PlanningAgent;
import com.client.problem.Request;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.client.problem.agent.SingleAgentAction;
import com.client.problem.goal.action.SingleGoalAction;
import com.client.problem.heuristics.SingleAgentHeuristics;
import com.search.IProblemAction;

public class MultiAgentPlaner {
	
	public static void initialize(WorldState initial) {
		HashMap<PlanningAgent, Integer> pathLengthEstimate = new HashMap<PlanningAgent, Integer>();
		for(Agent agent : initial.getAgents()) {
			PlanningAgent pAgent = new PlanningAgent(agent);
			pathLengthEstimate.put(pAgent, SingleAgentHeuristics.calculate(initial, agent));
		}
		Rigids.round_states.add(initial);
		AgentPriorities.initialize(pathLengthEstimate);
		//get all agents create minimum spanning tree or sth to determine which will have the shortest path
		//and assign priorities according to the path (the one with shortest path should get highest priority)
		//assign initial state
		// initialize AgentPriorities
	}
	
	//when the algorithm finishes its plan is in Rigids
	public static void plan() {
		boolean isPlanning;
		boolean canExecute;
		//SingleAgentAction saa = AgentPriorities.getHighestAgent().getNextBestAction();
		PlanningAgent pAgent;
		while(!isComplete()) {
			pAgent = AgentPriorities.getHighestAgent();
			if(pAgent.hasOwnNextAction()) {
				isPlanning = true;
				SingleAgentAction hpAction = null;
				while(isPlanning) {
					isPlanning = false;
					//ArrayList<WorldState> before = Rigids.getStateBackup();
					hpAction = pAgent.getNextBestAction();//this does not change the state
					if(hpAction.request != null) {
						//Rigids.restoreState(before);
						HashMap<PlanningAgent, LinkedList <SingleAgentAction> > response = getBiddingForRequest(hpAction.request, pAgent);//changes state
						applyAction(response);
						isPlanning = true;
					}
				}
				applyAction(hpAction, pAgent);
			}
			else {
				AgentPriorities.setPriority(pAgent, 0);
				continue;
			}
			for(int i = AgentPriorities.HIGHEST_PRIORITY - 1; i >= 0; i--) {
				//for each agent according to priorities
				boolean succededOnce = false;//whether successfully completed one action
				pAgent = AgentPriorities.getAgent(i);
				canExecute = true;
				while(canExecute) {
					if(pAgent.hasOwnNextAction()) {
						isPlanning = true;
						SingleAgentAction hpAction = null;
						Backup beforeExecution = getBackup();//so agent just started or already successfully planned one action (including requests)
						while(isPlanning) {
							isPlanning = false;
							//Backup beforeAction = getBackup();//is that needed?
							hpAction = pAgent.getNextBestAction();//this already changes the state
							if(hpAction.request != null) {
								//restore(beforeAction);
								HashMap<PlanningAgent, LinkedList <SingleAgentAction> > response = getBiddingForRequest(hpAction.request, pAgent);
								if(response != null) {
									applyAction(response);
									isPlanning = true;
								}
							}
							
						}
						if(hpAction.plan == null) {//when it exceeds cutoff or requests failed
							restore(beforeExecution);
							canExecute = false;
							//he cannot plan his move so he gets lowest
							//also it would be good if he tried next action
							if(succededOnce == false) {
								AgentPriorities.setPriority(pAgent, 0);//if agent could not plan a single action
								//i++;//in order to get back to agent with the same (new) priority
							}
								//might still be changed for example to check whether he requested help or not
						}
						else {
							applyAction(hpAction, pAgent);
							succededOnce = true;
						}
					}
					else {
						AgentPriorities.setPriority(pAgent, 0);
						canExecute = false;
					}
				}
			}
		}
	}
	
	public static HashMap<PlanningAgent, LinkedList <SingleAgentAction> > getBiddingForRequest(Request request, PlanningAgent callAgent) {
		ArrayList< HashMap<PlanningAgent, LinkedList <SingleAgentAction> > > biddings = new ArrayList<HashMap<PlanningAgent,LinkedList<SingleAgentAction>>>();
		boolean isPlanning;
		for(PlanningAgent pAgent : AgentPriorities.getPlanningAgents()) {
			if(request.agentInvolved != null && request.agentInvolved.id.equals(pAgent.agent.id))
				continue;//if it is agent that has to move then only he can present a plan for that
			HashMap<PlanningAgent, LinkedList <SingleAgentAction> > agentsPlans = new HashMap<PlanningAgent, LinkedList<SingleAgentAction>>();
			Backup beforePlanning = getBackup();
			if(callAgent.equals(pAgent))
				continue;//should not ever happen
			if(pAgent.agent.color.equals(request.color) && AgentPriorities.getPriority(pAgent) <= request.priority) {
				isPlanning = true;
				SingleAgentAction action = null;
				while(isPlanning) {
					isPlanning = false;
					//Backup beforeAction = getBackup();
					action = pAgent.planForRequest(request);//trying to plan for request
					if(action.request != null) {//planning solo failed; NULLIFY plan if making request
						//restore(beforeAction);//action did mess with the state so it has to be restored
						action.request.priority = request.priority;//here setting priority to the original calling agent (don't know whether it is good)
						HashMap<PlanningAgent, LinkedList <SingleAgentAction> > response = getBiddingForRequest(action.request, pAgent);
						if(response != null) {
							applyAction(response);//do we need that? YES!
							//BTW state must include round from which each agent begins planning
							isPlanning = true;
						}
					}
				}
				if(action.plan != null) {//no way to get to the goal (can happen if cutoff for non-highest priority is reached or requests failed!!!!)
					if(agentsPlans.containsKey(pAgent)) {
						agentsPlans.get(pAgent).addLast(action);//action of an agent is added as last because in some response this agent might have done something
					}
					else {
						LinkedList<SingleAgentAction> newList = new LinkedList<SingleAgentAction>();
						newList.addLast(action);
						agentsPlans.put(pAgent, newList);
					}
					biddings.add(agentsPlans);
				}
			}
			restore(beforePlanning);//state has to be restored so that other agents may plan
		}
		
		return getBestPlan(biddings);
	}
	
	private static HashMap<PlanningAgent, LinkedList <SingleAgentAction> > getBestPlan(ArrayList< HashMap<PlanningAgent, LinkedList <SingleAgentAction> > > biddings) {
		int min = Integer.MAX_VALUE;
		HashMap<PlanningAgent, LinkedList <SingleAgentAction> > best = null;
		for(HashMap<PlanningAgent, LinkedList <SingleAgentAction> > bid : biddings) {
			int max = 0;
			for(Entry<PlanningAgent, LinkedList<SingleAgentAction>> e : bid.entrySet()) {
				LinkedList<SingleAgentAction> plan = e.getValue();
				int sum = e.getKey().getLastRound();//initialize to current plan length
				for(SingleAgentAction action : plan) {
					sum+=action.plan.size();
				}
				if(sum > max)
					max = sum;
			}
			if(max < min) {
				min = max;
				best = bid;
			}
		}
		return best;
	}
	
	public static void applyAction(HashMap<PlanningAgent, LinkedList <SingleAgentAction> > actions) {
		for(Entry<PlanningAgent, LinkedList<SingleAgentAction>> e : actions.entrySet()) {
			int round = e.getKey().getLastRound()+1;//next round
			for(SingleAgentAction saa : e.getValue()) {
				for(IProblemAction action : saa.plan) {
					SingleGoalAction sga = (SingleGoalAction) action;
//					boolean success = sga.execute(Rigids.getWorldState(round));
//					if(!success)
//						System.err.println("Error in applyAction!");
					Rigids.getAgentPlan(e.getKey()).add(sga);
					round++;
				}
				Rigids.getReservedPlaces(round-1).addAll(saa.path);//or round-1?
			}
		}

		Rigids.resetWorldState();
		int round = 0;
		boolean wasChanged = true;
		while (wasChanged) {
			wasChanged = false;
			for(Entry<PlanningAgent, LinkedList<IProblemAction>> e : Rigids.agent_plan.entrySet()) {
				LinkedList<IProblemAction> plan = e.getValue();
				if(round < plan.size()) {
					wasChanged = true;
					SingleGoalAction sga = (SingleGoalAction) plan.get(round);
					boolean success = sga.execute(Rigids.getWorldState(round+1));
					if(!success)
						System.err.println("Error in applyAction!");
				}
			}
			round++;
		}
	}
	public static void applyAction(SingleAgentAction saa, PlanningAgent pAgent) {
		//TODO: applying action to the state according to all given plans
		int round = pAgent.getLastRound()+1;//next round
		for(IProblemAction action : saa.plan) {
			SingleGoalAction sga = (SingleGoalAction) action;
//			boolean success = sga.execute(Rigids.getWorldState(round));
//			if(!success) {
//				System.err.println("Error in applyAction!");
//				sga.isExecutable(Rigids.getWorldState(round));
//			}
			
			Rigids.getAgentPlan(pAgent).add(sga);
			round++;
		}
		Rigids.getReservedPlaces(round-1).addAll(saa.path);//or round-1?
		
		Rigids.resetWorldState();
		round = 0;
		boolean wasChanged = true;
		while (wasChanged) {
			wasChanged = false;
			for(Entry<PlanningAgent, LinkedList<IProblemAction>> e : Rigids.agent_plan.entrySet()) {
				LinkedList<IProblemAction> plan = e.getValue();
				if(round < plan.size()) {
					wasChanged = true;
					SingleGoalAction sga = (SingleGoalAction) plan.get(round);
					boolean success = sga.execute(Rigids.getWorldState(round+1));
					if(!success)
						System.err.println("Error in applyAction!");
				}
			}
			round++;
		}
	}
	
	private static boolean isComplete() {
		boolean isComplete = true;
		for(PlanningAgent pAgent : AgentPriorities.getPlanningAgents()) {
			if(pAgent.hasOwnNextAction())
				isComplete = false;
		}
		return isComplete;
	}
	
	public static Backup getBackup() {
		Backup backup = new Backup();
		backup.cutOffs = Rigids.getPlansBackup();
		backup.previousReserved = Rigids.getReservedBackup();
		backup.previousState = Rigids.getStateBackup();
		return backup;
	}
	
	public static void restore(Backup backup) {
		Rigids.restorePlans(backup.cutOffs);
		Rigids.restoreReserved(backup.previousReserved);
		Rigids.restoreState(backup.previousState);
	}
}
