package com.client.problem.goal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Position;
import com.client.problem.Direction;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.client.problem.goal.action.MoveAction;
import com.client.problem.goal.action.NoOpAction;
import com.client.problem.goal.action.PullAction;
import com.client.problem.goal.action.PushAction;
import com.client.problem.goal.action.SingleGoalAction;
import com.search.IProblemAction;

public class ActionsFactory {

	public static LinkedList<IProblemAction> getActions(WorldState state, Agent agent, SingleGoalAction prevAction, boolean isRelaxed) {
		LinkedList<IProblemAction> result = new LinkedList<IProblemAction>();
		
		Agent sa = (Agent) state.getAgent(agent).clone();
		WorldState future = (WorldState) state.clone();//changed by other agents in this round
		future.round= future.round+1;
		if(!isRelaxed){
			for(LinkedList<IProblemAction> plan : Rigids.agent_plan.values()) {
				if(state.round < plan.size()) {
					SingleGoalAction sga = (SingleGoalAction) plan.get(state.round);
					if(state.getAgent(sga.agent) != null)//TODO: is that enough?
						sga.execute(future);
				}
			}
		}
		
		HashSet<Position> reservedPositions = new HashSet<Position>();
		if(!isRelaxed)
			for(int i = future.round - 1; i < Rigids.reserved_places.size(); i++) {
				reservedPositions.addAll(Rigids.reserved_places.get(i));//reserved are all that are used in future
				//on those places boxes cannot be put
				//boxes that are on those places cannot be moved
			}
		
		
		boolean isOnReserved = false;
		//cannot move away from box leaving it on reserved path
		Box lastMovedBox = null;
		if(prevAction instanceof PullAction) {
			PullAction pAct = (PullAction) prevAction;
			if(reservedPositions.contains(pAct.box.pos))
				isOnReserved = true;
			lastMovedBox = pAct.box;
		}
		else if(prevAction instanceof PushAction) {
			PushAction pAct = (PushAction) prevAction;
			if(reservedPositions.contains(pAct.box.pos))
				isOnReserved = true;
			lastMovedBox = pAct.box;
		}

		HashMap<Direction, Position> poses = Rigids.nearbyPositions(sa.pos);
		if(!isOnReserved) {
			for (Direction agentDir : poses.keySet()) {
				Position toTake = poses.get(agentDir);
				if (state.isFree(poses.get(agentDir)) && future.isFree(toTake)) {
					MoveAction move = new MoveAction(sa, agentDir, sa.pos, toTake);
					move.target = new WorldState(future);
					if(move.execute(move.target))
					{
						if(canOthersExecute(move.target, isRelaxed))
							result.add(move);
					}
				}
				else if (state.isColorFree(toTake, sa)
						&& future.isColorFree(toTake, sa)
						&& !reservedPositions.contains(toTake)) {//when the colliding object is agent or box of different color
					MoveAction move = new MoveAction(sa, agentDir, sa.pos, toTake);
					move.target = new WorldState(future);
					if(move.colorExecute(move.target))
					{
						if(canOthersExecute(move.target, isRelaxed))
							result.add(move);
					}
					move.conflictingObject = future.getStateObject(toTake);
					move.stepCost=3;
				}
			}
			NoOpAction noOp = new NoOpAction(sa, sa.pos);
			noOp.target = new WorldState(future);
			if(noOp.execute(noOp.target))
			{
				if(canOthersExecute(noOp.target, isRelaxed))
					result.add(noOp);
			}
		}

		HashMap<Direction, Box> boxes = future.nearbyBoxes(sa.pos);
		for (Direction agentToBoxDir : boxes.keySet()) {
			Box b = boxes.get(agentToBoxDir);
			if(isOnReserved && b.masterID != lastMovedBox.masterID)
				continue;//do not push/pull other box if current is on reserved
			if(reservedPositions.contains(b.pos))
				continue;//do not push/pull the box that is on reserved path
			if (sa.color.equals(b.color)) {
				// add push
				HashMap<Direction, Position> bAround = Rigids.nearbyPositions(b.pos);
				for (Direction boxDir : bAround.keySet()) {
					Position toTake = bAround.get(boxDir);
					if (state.isFree(toTake) && future.isFree(toTake)) {
						PushAction push = new PushAction(agent, b, agentToBoxDir, boxDir, sa.pos, toTake);
						push.target = new WorldState(future);
						if(push.execute(push.target))
						{
							if(canOthersExecute(push.target, isRelaxed));
							result.add(push);
						}
					}
					else if (state.isColorFree(toTake, sa)
							&& future.isColorFree(toTake, sa)
							&& !reservedPositions.contains(toTake)) {
						PushAction push = new PushAction(agent, b, agentToBoxDir, boxDir, sa.pos, toTake);
						push.target = new WorldState(future);
						if(push.colorExecute(push.target))
						{
							if(canOthersExecute(push.target, isRelaxed));
							result.add(push);
						}
						push.conflictingObject = future.getStateObject(toTake);
						push.stepCost=3;
					}
				}

				// add pull
				HashMap<Direction, Position> aAround = Rigids.nearbyPositions(sa.pos);
				for (Direction pullDir : aAround.keySet()) {
					Position toTake = aAround.get(pullDir);
					if (state.isFree(toTake) && future.isFree(toTake)) {
						PullAction pull = new PullAction(agent, b, pullDir, agentToBoxDir, b.pos, toTake);
						pull.target = new WorldState(future);
						if(pull.execute(pull.target))
						{
							if(canOthersExecute(pull.target, isRelaxed))
								result.add(pull);
						}
					}
					else if (state.isColorFree(toTake, sa)
							&& future.isColorFree(toTake, sa)
							&& !reservedPositions.contains(toTake)) {
						PullAction pull = new PullAction(agent, b, pullDir, agentToBoxDir, b.pos, toTake);
						pull.target = new WorldState(future);
						if(pull.colorExecute(pull.target))
						{
							if(canOthersExecute(pull.target, isRelaxed))
								result.add(pull);
						}
						pull.conflictingObject = future.getStateObject(toTake);
						pull.stepCost=3;
					}
				}
			}
		}

		return result;
	}
	
	private static boolean canOthersExecute(WorldState state, boolean isRelaxed) {
		if(isRelaxed)
			return true;
		boolean canExecute = true;
		for(LinkedList<IProblemAction> plan : Rigids.agent_plan.values()) {
			if(state.round < plan.size()) {
				SingleGoalAction sga = (SingleGoalAction) plan.get(state.round);
				if(!sga.isExecutable(state))
					canExecute = false;
			}
		}
		return canExecute;
	}

}
