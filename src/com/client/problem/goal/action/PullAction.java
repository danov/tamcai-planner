package com.client.problem.goal.action;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Position;
import com.client.problem.Direction;
import com.client.problem.Rigids;
import com.client.problem.WorldState;

public class PullAction extends SingleGoalAction {

	public Direction agentDir;
	public Direction boxDir;
	public Box box;

	public PullAction(Agent agent, Box b, Direction agentDir, Direction boxDir, Position releasePos, Position takePos) {
		this.agentDir = agentDir;
		this.boxDir = boxDir;
		this.box = new Box(b);
		this.agent = new Agent(agent);
		this.oldPos = new Position(releasePos);
		this.pos = new Position(takePos);
	}

	@Override
	public boolean isExecutable(WorldState target) {
		return target.getAgent(agent) != null && target.getBox(box) != null && target.isFree(pos) && target.getBox(box).pos.equals(oldPos)
				&& Rigids.getDirection(target.getAgent(agent).pos, target.getBox(box).pos).equals(boxDir)
				&& Rigids.getDirection(target.getAgent(agent).pos, pos).equals(agentDir);
	}

	@Override
	public boolean execute(WorldState target) {
		if (isExecutable(target)) {
			Position apos = target.getAgent(agent).pos;
			target.setAgentPos(agent, pos);
			target.setBoxPos(box, apos);
			return true;
		} else return false;
	}

	@Override
	public boolean colorExecute(WorldState target) {
		if (target.getAgent(agent) != null && target.getBox(box) != null && target.isColorFree(pos, agent) && target.getBox(box).pos.equals(oldPos)
				&& Rigids.getDirection(target.getAgent(agent).pos, target.getBox(box).pos).equals(boxDir)
				&& Rigids.getDirection(target.getAgent(agent).pos, pos).equals(agentDir)) {
			Position apos = target.getAgent(agent).pos;
			target.setAgentPos(agent, pos);
			target.setBoxPos(box, apos);
			return true;
		} else return false;
	}

	@Override
	public String toString() {
		return "Pull(" + agentDir + "," + boxDir + ")";
	}

}
