package com.client.problem.goal.action;

import com.client.objects.Agent;
import com.client.objects.Position;
import com.client.objects.StateObject;
import com.client.problem.WorldState;
import com.search.IProblemAction;

public abstract class SingleGoalAction implements IProblemAction {

	public final static int baseStepCost = 1;
	
	public int stepCost = baseStepCost;
	
	public int heuristicCost = 0;
	
	public Agent agent;
	public Position pos;
	public Position oldPos;
	
	public WorldState target;
	public StateObject conflictingObject;

	public abstract boolean isExecutable(WorldState target);

	public abstract boolean execute(WorldState target);

	public abstract boolean colorExecute(WorldState target);
}
