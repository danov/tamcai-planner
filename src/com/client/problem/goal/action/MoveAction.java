package com.client.problem.goal.action;

import com.client.objects.Agent;
import com.client.objects.Position;
import com.client.problem.Direction;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.search.IProblemAction;

public class MoveAction extends SingleGoalAction implements IProblemAction {

	public Direction moveDirection;

	public MoveAction(Agent agent, Direction dir, Position freePos, Position takePos) {
		this.moveDirection = dir;
		this.agent = new Agent(agent);
		this.oldPos = new Position(freePos);
		this.pos = new Position(takePos);
	}

	@Override
	public boolean isExecutable(WorldState target) {
		return target.getAgent(agent) != null && target.isFree(pos) && Rigids.getDirection(target.getAgent(agent).pos, pos).equals(moveDirection);
	}

	@Override
	public boolean execute(WorldState target) {
		if (isExecutable(target)) {
			target.setAgentPos(agent, pos);
			return true;
		} else return false;
	}

	@Override
	public String toString() {
		return "Move(" + moveDirection + ")";
	}

	@Override
	public boolean colorExecute(WorldState target) {
		if (target.getAgent(agent) != null && target.isColorFree(pos, agent) && Rigids.getDirection(target.getAgent(agent).pos, pos).equals(moveDirection)) {
			target.setAgentPos(agent, pos);
			return true;
		} else return false;
	}
}
