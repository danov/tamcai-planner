package com.client.problem.goal.action;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Position;
import com.client.problem.Direction;
import com.client.problem.Rigids;
import com.client.problem.WorldState;

public class PushAction extends SingleGoalAction {

	public Direction agentDir;
	public Direction boxDir;
	public Box box;

	public PushAction(Agent agent, Box b, Direction agentDir, Direction boxDir, Position releasePos, Position takePos) {
		this.agentDir = agentDir;
		this.boxDir = boxDir;
		this.box = new Box(b);
		this.agent = new Agent(agent);
		this.oldPos = new Position(releasePos);
		this.pos = new Position(takePos);
	}

	@Override
	public boolean isExecutable(WorldState target) {
		return target.getAgent(agent) != null && target.getBox(box) != null && target.isFree(pos) && target.getAgent(agent).pos.equals(oldPos)
				&& Rigids.getDirection(oldPos, target.getBox(box).pos).equals(agentDir) && Rigids.getDirection(target.getBox(box).pos, pos).equals(boxDir);
	}

	@Override
	public boolean execute(WorldState target) {
		if (isExecutable(target)) {
			Position bpos = target.getBox(box).pos;
			target.setBoxPos(box, pos);
			target.setAgentPos(agent, bpos);
			return true;
		} else return false;
	}

	@Override
	public boolean colorExecute(WorldState target) {
		if (target.getAgent(agent) != null && target.getBox(box) != null && target.isColorFree(pos, agent) && target.getAgent(agent).pos.equals(oldPos)
				&& Rigids.getDirection(oldPos, target.getBox(box).pos).equals(agentDir) && Rigids.getDirection(target.getBox(box).pos, pos).equals(boxDir)) {
			Position bpos = target.getBox(box).pos;
			target.setBoxPos(box, pos);
			target.setAgentPos(agent, bpos);
			return true;
		} else return false;
	}

	@Override
	public String toString() {
		return "Push(" + agentDir + "," + boxDir + ")";
	}

}
