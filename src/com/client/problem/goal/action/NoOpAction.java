package com.client.problem.goal.action;

import com.client.objects.Agent;
import com.client.objects.Position;
import com.client.problem.WorldState;
import com.search.IProblemAction;

public class NoOpAction extends SingleGoalAction implements IProblemAction {

	public NoOpAction(Agent agent, Position pos) {
		this.agent = new Agent(agent);
		this.oldPos = new Position(pos);
		this.pos = new Position(pos);
	}

	@Override
	public boolean isExecutable(WorldState target) {
		return target.getAgent(pos).id.equals(agent.id);
	}

	@Override
	public boolean execute(WorldState target) {
		return isExecutable(target);
	}

	@Override
	public boolean colorExecute(WorldState target) {
		return true;
	}

	@Override
	public String toString() {
		return "NoOp";
	}
}
