package com.client.problem.goal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.objects.Position;
import com.client.objects.StateObject;
import com.client.problem.AgentPriorities;
import com.client.problem.BigActionType;
import com.client.problem.Direction;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.client.problem.goal.action.*;
//import com.client.problem.goal.action.SingleGoalAction;
import com.client.problem.heuristics.NearestFreeHeuristics;
import com.client.problem.heuristics.SkipObstacle;
import com.search.AbstractSearchProblem;
import com.search.IProblemAction;
import com.search.IProblemState;
import com.search.SearchNode;

public class SingleGoalProblem extends AbstractSearchProblem {

	private Agent agent;
	private Box box;
	private Goal goal;
	private Direction dir;
	private WorldState initial;
	private BigActionType type;
	private LinkedHashMap<StateObject, Position> objFreePlace = new LinkedHashMap<StateObject, Position>();
	public int alarmValue = 10000;
	private LinkedHashSet<Position> goToPath;
	private LinkedHashSet<Position> movePath;
	public Agent conflictingAgent;
	public ArrayList<Box> conflictingBoxes;
	//this one will count how many times the obstacle has been encountered and and assumed skippable
	private HashMap<StateObject, Integer> times_encountered = new HashMap<StateObject, Integer>();
	public int priority;
	public boolean isRelaxed;
	/**
	 * 
	 * @param initial
	 * @param type
	 *            PUSHTO_GOAL, PULLTO_GOAL
	 * @param a
	 * @param b
	 * @param g
	 */
	public SingleGoalProblem(WorldState initial, BigActionType type, Direction dir, Agent a, Box b, Goal g) {
		this.initial = (WorldState) initial.clone();
		this.agent = (Agent) a.clone();
		this.box = (Box) b.clone();
		this.goal = (Goal) g.clone();
		this.dir = dir;
		this.type = type;
	}

	//without direction
	public SingleGoalProblem(WorldState initial, BigActionType type, Agent a, Box b, Goal g) {
		this.initial = (WorldState) initial.clone();
		this.agent = (Agent) a.clone();
		this.box = (Box) b.clone();
		this.goal = (Goal) g.clone();
		this.type = type;
	}
	/**
	 * 
	 * @param initial
	 * @param type
	 *            GOTO_BOX
	 * @param a
	 * @param b
	 */
	public SingleGoalProblem(WorldState initial, BigActionType type, Direction dir, Agent a, Box b) {
		this.initial = (WorldState) initial.clone();
		this.agent = (Agent) a.clone();
		this.box = (Box) b.clone();
		this.dir = dir;
		this.type = type;
	}
	
	//move away to free place problem
	public SingleGoalProblem(WorldState initial, BigActionType type, Agent a, LinkedHashMap<StateObject, Position> objFreePlace, LinkedHashSet<Position> movePath) {
		this.initial = (WorldState) initial.clone();
		this.agent = (Agent) a.clone();
		this.objFreePlace = objFreePlace;
		this.type = type;
		this.movePath = movePath;
	}

	@Override
	public boolean goalTest(SearchNode node) {
		WorldState ws = (WorldState) node.state;
		SingleGoalAction sga = (SingleGoalAction) node.action;
		if(sga != null && sga.conflictingObject != null) {
			StateObject obj = sga.conflictingObject; 
			if(times_encountered.containsKey(obj))
				times_encountered.put(obj, times_encountered.get(obj));
			else
				times_encountered.put(obj, 1);//counter has to be increased only when a state is explored
		}
		
		boolean result = false;
		switch (this.type) {
			case GOTO_BOX:
				result = ws.distance(agent, box) == 1;
				break;
			case CARRYTO_BOX:
				result = ws.distance(agent, box) == 1 && ws.distance(agent, box) == 1
						;//&& Rigids.getDirection(ws.getBox(box).pos, ws.getAgent(agent).pos) == dir; if uncommented check if direction is not null!
				break;
			case CARRYTO_GOAL:
				result = goal.equals(ws.getBox(box)) && ws.distance(agent, box) == 1
						;//&& Rigids.getDirection(ws.getBox(box).pos, ws.getAgent(agent).pos) == dir;
				break;
			case MOVE_AWAY:
				result = NearestFreeHeuristics.isPathFree(ws, movePath, objFreePlace);
		}
		if(result) {
			goToPath = null;//just resetting to get rid of unnecessary things
		}
		if (result && sga != null) sga.target = (WorldState) node.state;
		if(!isRelaxed && AgentPriorities.HIGHEST_PRIORITY != priority &&//so that plan with highest priority does not cut off
				!result && ws.round > AgentPriorities.getHigherAgent(priority).getLastRound() - 2) {
			result = true;
			cutOffFlag = true;
		}
		return result;
	}

	@Override
	public SearchNode initialState() {
		SearchNode nsn = new SearchNode();
		nsn.parent = null;
		nsn.pathCost = 0;
		nsn.state = initial;
		nsn.action = null;
		nsn.heuristicCost = calculateHeuristics(nsn.state);
		return nsn;
	}

	@Override
	public LinkedList<IProblemAction> getActions(SearchNode node) {
		LinkedList<IProblemAction> result = ActionsFactory.getActions((WorldState) node.state, agent, (SingleGoalAction)node.action, isRelaxed);

		if (node.state != initialState().state) ((WorldState) node.state).clean();

		if ((exploredStates % alarmValue) == 0 && exploredStates > 0) {
			System.err.println("Alert!!! SingleGoalProblem -> " + "Explored: " + exploredStates);
		}

		if ((frontierMaximum % alarmValue) == 0 && frontierMaximum > 0) {
			System.err.println("Alert!!! SingleGoalProblem -> " + "Frontier: " + frontierMaximum);
		}

		return result;
	}

	@Override
	public SearchNode childNode(SearchNode node, IProblemAction action) {
		SingleGoalAction sga = (SingleGoalAction) action;
		SearchNode nsn = new SearchNode();
		
		nsn.parent = node;
		nsn.pathCost = node.pathCost + sga.stepCost;
		nsn.state = sga.target;
		nsn.action = sga;
		nsn.heuristicCost = calculateHeuristics(nsn.state);

		if(sga.conflictingObject != null) {
			StateObject obj = sga.conflictingObject;
			WorldState ws = (WorldState) node.state;
			boolean canSkip = canSkip(ws, type, ws.getAgent(agent), ws.getBox(box), goal, obj);
			if(canSkip && (!times_encountered.containsKey(obj) || times_encountered.get(obj) < times_encountered.size())) {
				nsn.pathCost = Integer.MAX_VALUE/2;//assure that this won't be selected in search
			}
			else if(obj instanceof Agent && conflictingAgent == null)
				if(conflictingBoxes == null || conflictingBoxes.get(0).color.equals(obj.color))//check for colors
					conflictingAgent = (Agent) obj;//adding only one (first) agent
			else if(obj instanceof Box) {
				if(conflictingAgent.color.equals(obj.color)) {//check for colors
					if(conflictingBoxes == null) {
						conflictingBoxes = new ArrayList<Box>();
						conflictingBoxes.add((Box) obj);
					}
					else if(conflictingBoxes.get(0).color.equals(obj)) {
						conflictingBoxes.add((Box) obj);//list must contain boxes of the same color
					}
				}
			}
		}
		sga.heuristicCost = nsn.heuristicCost;
		//sga.target = null; //TODO: What is going on here? Either way it cannot stay like that or has to be changed
		//sga.execute((WorldState) nsn.state);

		if (!(sga instanceof MoveAction) && type == BigActionType.GOTO_BOX) {
			//nsn.pathCost++;
		}
		if(sga instanceof PullAction) {
			PullAction pa = (PullAction) sga;
			if(box.masterID != pa.box.masterID && ((WorldState)node.state).isOnGoal(pa.box))
				nsn.pathCost+=3;
		}
		
		if(sga instanceof PushAction) {
			PushAction pa = (PushAction) sga;
			if(box.masterID != pa.box.masterID && ((WorldState)node.state).isOnGoal(pa.box))
				nsn.pathCost+=3;
		}
		return nsn;
	}
	public void establishHeuristics() {
		if(objFreePlace.isEmpty()) {
			//We calculate separately going to box which path will be used in NearestFreeProblem
			ArrayList<Box> higherPriorityBoxes = Rigids.getBoxesOnGoalsAboveLevel(initial, Rigids.getPriority(goal));
			goToPath = NearestFreeHeuristics.getGoToPath(initial, agent, box,
					goal, higherPriorityBoxes);
			objFreePlace = NearestFreeHeuristics.establishHeuristics(initial, type, this.agent, this.box, this.goal, goToPath);
		}
	}

	public int calculateHeuristics(IProblemState state) {
		WorldState ws = (WorldState) state;
		int heuristics = 0;
		switch (type) {
			case GOTO_BOX:
				break;
			case CARRYTO_BOX:
				// Ensuring Hadd, because it is admissible in case of static box2
				heuristics = SingleGoalAction.baseStepCost * (ws.distance(box, box) - 3);
				break;
			case CARRYTO_GOAL:
				// Hadd, because the goal is always static and the agent has to go to the box, so he can push it/pull it
				heuristics = SingleGoalAction.baseStepCost * (ws.distance(box, goal) - 1);
				break;
			case MOVE_AWAY:
				heuristics = NearestFreeHeuristics.calculate(ws, objFreePlace, agent);
				break;
		}

		heuristics += NearestFreeHeuristics.calculate(ws, objFreePlace, agent, box, goToPath);
		//heuristics += SingleAgentHeuristics.calculate(ws, agent);
		//heuristics += RelaxedGoalHeuristics.calculate((WorldState) state, type, agent, box1, box2, goal);
		return heuristics;
	}
	
	private boolean canSkip(WorldState state, BigActionType type, Agent a, Box b, Goal g, StateObject conflictingObject) {
		WorldState relaxed = state.getRelaxed(a, b, new LinkedList<Box>());
		if(type == BigActionType.GOTO_BOX && Rigids.distance(a.pos, b.pos) < 2)
			return true;
		else if(type == BigActionType.CARRYTO_GOAL && (Rigids.distance(a.pos, b.pos) + Rigids.distance(b.pos, g.pos) < 3))
			return true;
		return SkipObstacle.canSkip(relaxed, a, b, g, type, conflictingObject, false);
	}

	public WorldState getFinalState(LinkedList<IProblemAction> plan) {
		if (plan != null && !plan.isEmpty()) return ((SingleGoalAction) plan.getLast()).target;
		else return initial;
	}

	@Override
	public String toString() {
		return "SingleGoalProblem {" + type + ", " + agent + ", " + box + ", " + goal + ", " + initial + ", " + "}";
	}
}
