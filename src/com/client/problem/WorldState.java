package com.client.problem;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.objects.Position;
import com.client.objects.StateObject;
import com.search.IProblemState;
import com.search.util.ILongHashCode;

public class WorldState implements IProblemState, ILongHashCode {

	protected HashMap<Character, Agent> agents = new HashMap<Character, Agent>(2);
	protected Map<Position, Agent> agentsByPos = new HashMap<Position, Agent>(2);

	protected Map<Character, ArrayList<Box>> boxesByID = new HashMap<Character, ArrayList<Box>>(2);
	protected Map<String, ArrayList<Box>> boxesByCol = new HashMap<String, ArrayList<Box>>(2);

	protected Map<Integer, Box> boxesByMasterID = new HashMap<Integer, Box>(2);
	protected Map<Position, Box> boxesByPos = new HashMap<Position, Box>(2);
	public int round;

	protected long agentsHash = 0;
	protected long boxesHash = 0;

	private static long longPrime;
	private static long longBase;

	private static int prime = 0;
	private static int base = 0;

	public WorldState(Collection<Agent> agents, Collection<Box> boxes) {
		if (prime == 0) {
			BigInteger generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			prime = generator.intValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			base = generator.intValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			longPrime = generator.longValue();

			generator = new BigInteger(32, 12, new Random(System.currentTimeMillis()));
			longBase = generator.longValue();
		}

		Iterator<Agent> it = agents.iterator();
		while (it.hasNext()) {
			Agent a = it.next();
			agentsHash += a.longHashCode();
			this.agents.put(a.id, a);
			this.agentsByPos.put(a.pos, a);
		}

		Iterator<Box> jt = boxes.iterator();
		while (jt.hasNext()) {
			Box b = jt.next();
			boxesHash += b.longHashCode();
			this.boxesByMasterID.put(b.masterID, b);
			this.boxesByPos.put(b.pos, b);
		}

	}

	public WorldState(WorldState ws, Agent a, Box b, Collection<Box> hpBoxes) {
		Agent agent = (Agent) ws.getAgent(a).clone();
		Box box = (Box) ws.getBox(b).clone();
		this.agentsHash += agent.longHashCode();
		this.agents.put(agent.id, agent);
		this.agentsByPos.put(agent.pos, agent);
		if (b != null) {
			this.boxesHash += box.longHashCode();
			this.boxesByMasterID.put(box.masterID, box);
			this.boxesByPos.put(box.pos, box);
		}
		for (Box hpBox : hpBoxes) {
			Box hp = (Box) ws.getBox(hpBox).clone();
			this.boxesHash += hp.longHashCode();
			this.boxesByMasterID.put(hp.masterID, hp);
			this.boxesByPos.put(hp.pos, hp);
		}
		this.round = ws.round;
	}

	public WorldState(WorldState ws) {
		this(ws.agents.values(), ws.boxesByPos.values());
		this.round = ws.round;
	}

	public WorldState getRelaxed(Agent a, Box b, Collection<Box> hpBoxes) {
		return new WorldState(this, a, b, hpBoxes);
	}

	public void chooseAgent(Agent agent) {
		if (getAgent(agent) != null) {
			Agent a = getAgent(agent);
			this.agents = new HashMap<Character, Agent>(2);
			this.agentsByPos = new HashMap<Position, Agent>(2);
			agentsHash = a.longHashCode();
			this.agents.put(a.id, a);
			this.agentsByPos.put(a.pos, a);

			HashMap<Integer, Box> newBoxesByMasterID = new HashMap<Integer, Box>(2);
			HashMap<Position, Box> newBoxesByPos = new HashMap<Position, Box>(2);

			boxesHash = 0;
			Iterator<Box> jt = boxesByMasterID.values().iterator();
			while (jt.hasNext()) {
				Box b = jt.next();
				if (b.color.equals(getAgent(agent).color)) {
					boxesHash += b.longHashCode();
					newBoxesByMasterID.put(b.masterID, b);
					newBoxesByPos.put(b.pos, b);
				}
			}

			boxesByMasterID = newBoxesByMasterID;
			boxesByPos = newBoxesByPos;

			if (!boxesByCol.isEmpty()) boxesByCol = new HashMap<String, ArrayList<Box>>(2);
			if (!boxesByID.isEmpty()) boxesByID = new HashMap<Character, ArrayList<Box>>(2);
		}
	}

	public void setAgentPos(Agent a, Position p) {
		if (agents.containsKey(a.id) && isFree(p)) {
			Agent newAgent = new Agent(getAgent(a));
			agentsHash -= newAgent.longHashCode();
			agentsByPos.remove(newAgent.pos);

			newAgent.pos = new Position(p);
			agents.put(newAgent.id, newAgent);
			agentsByPos.put(newAgent.pos, newAgent);
			agentsHash += getAgent(a).longHashCode();
		}
	}

	public Agent getAgent(Character id) {
		return agents.get(id);
	}

	public Agent getAgent(Agent a) {
		return getAgent(a.id);
	}

	public Agent getAgent(Position p) {
		return agentsByPos.get(p);
	}

	public ArrayList<Agent> getAgents() {
		ArrayList<Agent> arr = new ArrayList<Agent>(agents.values());
		Collections.sort(arr);
		return arr;
	}

	public void setBoxPos(Box b, Position p) {
		if (boxesByMasterID.containsKey(b.masterID) && isFree(p)) {
			if (!boxesByCol.isEmpty()) boxesByCol = new HashMap<String, ArrayList<Box>>(2);
			if (!boxesByID.isEmpty()) boxesByID = new HashMap<Character, ArrayList<Box>>(2);

			Box newBox = new Box(getBox(b));
			boxesHash -= newBox.longHashCode();
			boxesByPos.remove(newBox.pos);

			newBox.pos = new Position(p);
			boxesByPos.put(newBox.pos, newBox);
			boxesByMasterID.put(newBox.masterID, newBox);
			boxesHash += newBox.longHashCode();
		}
	}

	public Collection<String> getAllColors() {
		return boxesByCol.keySet();
	}

	public Box getBox(Position pos) {
		return boxesByPos.get(pos);
	}

	public Box getBox(int masterID) {
		return boxesByMasterID.get(masterID);
	}

	public Box getBox(Box b) {
		return getBox(b.masterID);
	}

	public Collection<Box> getBoxes() {
		return boxesByMasterID.values();
	}

	public ArrayList<Box> getBoxes(Character id) {

		if (this.boxesByID.isEmpty()) {
			for (Box b : this.boxesByMasterID.values()) {
				if (this.boxesByID.containsKey(b.id)) {
					this.boxesByID.get(b.id).add(b);
				} else {
					ArrayList<Box> pq = new ArrayList<Box>();
					pq.add(b);
					this.boxesByID.put(b.id, pq);
				}
			}
		}

		return boxesByID.get(id);
	}

	public ArrayList<Box> getBoxes(String color) {

		if (this.boxesByCol.isEmpty()) {
			for (Box b : this.boxesByMasterID.values()) {
				if (this.boxesByCol.containsKey(b.color)) {
					this.boxesByCol.get(b.color).add(b);
				} else {
					ArrayList<Box> pq = new ArrayList<Box>();
					pq.add(b);
					this.boxesByCol.put(b.color, pq);
				}
			}
		}

		return boxesByCol.get(color);
	}

	public HashMap<Direction, Box> nearbyBoxes(Position pos) {
		HashMap<Direction, Box> map = new HashMap<Direction, Box>();
		if (pos == null) return map;

		HashMap<Direction, Position> poses = Rigids.nearbyPositions(pos);
		for (Direction m : poses.keySet()) {
			Box b = getBox(poses.get(m));
			if (b != null) map.put(m, b);
		}

		return map;
	}

	public HashMap<Direction, Box> nearbyBoxes(Agent agent) {
		HashMap<Direction, Box> map = new HashMap<Direction, Box>();
		if (agent == null) return map;

		HashMap<Direction, Position> poses = Rigids.nearbyPositions(agent.pos);
		for (Direction m : poses.keySet()) {
			Box b = getBox(poses.get(m));
			if (b != null && b.color.equals(agent.color)) map.put(m, b);
		}

		return map;
	}

	public int distance(Agent a1, Agent a2) {
		return Rigids.distance(getAgent(a1).pos, getAgent(a2).pos);
	}

	public int distance(Agent a1, Box b2) {
		return Rigids.distance(getAgent(a1).pos, getBox(b2).pos);
	}

	public int distance(Box b1, Agent a2) {
		return Rigids.distance(getBox(b1).pos, getAgent(a2).pos);
	}

	public int distance(Box b1, Box b2) {
		return Rigids.distance(getBox(b1).pos, getBox(b2).pos);
	}

	public int distance(Box b1, Goal g2) {
		return Rigids.distance(getBox(b1).pos, g2.pos);
	}

	public int distance(Goal g1, Box b2) {
		return Rigids.distance(g1.pos, getBox(b2).pos);
	}

	public int distance(Goal g1, Goal g2) {
		return Rigids.distance(g1.pos, g2.pos);
	}

	public int distance(Agent a1, Goal g2) {
		return Rigids.distance(getAgent(a1).pos, g2.pos);
	}

	public int distance(Goal g1, Agent a2) {
		return Rigids.distance(g1.pos, getAgent(a2).pos);
	}

	public boolean isOnGoal(Box b) {
		return getBox(b) != null && Rigids.getGoal(getBox(b).pos) != null && Rigids.getGoal(getBox(b).pos).equals(b);
	}

	public boolean isAchieved(Goal g) {
		return boxesByPos.containsKey(g.pos) && boxesByPos.get(g.pos).equals(g);
	}

	public Boolean isBottleNeck(Position pos) {
		int counter = 0;
		HashMap<Direction, Position> poses = Rigids.nearbyPositions(pos);
		for (Direction m : poses.keySet()) {
			if (isFree(poses.get(m))) {
				if (counter > 1) return false;
				else counter++;
			}
		}
		return true;
	}

	public Boolean isFree(Position pos) {
		return !Rigids.isWall(pos) && !boxesByPos.containsKey(pos) && !agentsByPos.containsKey(pos);
	}

	//returns true when colliding box is different color or it is an agent
	public Boolean isColorFree(Position pos, Agent a) {
		return !Rigids.isWall(pos) && (!boxesByPos.containsKey(pos) || !boxesByPos.get(pos).color.equals(a.color))
				&& (!agentsByPos.containsKey(pos) || !agentsByPos.get(pos).id.equals(a.id));
	}

	public Boolean isAgent(Position pos) {
		return getAgent(pos) != null;
	}

	public Boolean isDiffColorBox(Agent a, Position pos) {
		return getBox(pos) != null && getAgent(a) != null && !getBox(pos).color.equals(getAgent(a).color);
	}

	public boolean isDifferentColorObject(String color, Position pos) {
		if (color == null) {
			return false;
		}

		Box box = getBox(pos);
		if (box != null && !box.color.equals(color)) {
			return true;
		}

		Agent agent = getAgent(pos);
		if (agent != null && !agent.color.equals(color)) {
			return true;
		}

		return false;
	}

	public StateObject getStateObject(Position pos) {
		Box box = getBox(pos);
		if (box != null) {
			return box;
		}
		Agent agent = getAgent(pos);
		return agent;

	}

	public StateObject getStateObject(StateObject obj) {
		if (obj == null) {
			return null;
		}

		if (obj instanceof Agent) {
			return getAgent((Agent) obj);
		}

		if (obj instanceof Box) {
			return getBox((Box) obj);
		}
		return null;
	}

	public Boolean isFree(int x, int y) {
		return isFree(new Position(x, y));
	}

	@Override
	public Object clone() {
		return new WorldState(this);
	}

	@Override
	public boolean equals(Object o) {
		WorldState temp = (WorldState) o;
		if (o != null && o.getClass().equals(getClass())) return agents.equals(temp.agents) && boxesByMasterID.equals(temp.boxesByMasterID);
		else return false;
	}

	@Override
	public String toString() {
		return ("State {" + agents.values() + ", " + boxesByMasterID.values() + "}");
	}

	@Override
	public int hashCode() {
		int result = base;
		result = prime * result + ((agents == null) ? 0 : agents.hashCode());
		result = prime * result + ((boxesByMasterID == null) ? 0 : boxesByMasterID.hashCode());
		return result;
	}

	@Override
	public long longHashCode() {
		long result = longBase;
		result = longPrime * result + agentsHash;
		result = longPrime * result + ((agents == null) ? 0 : agents.hashCode());
		result = longPrime * result + boxesHash;
		result = longPrime * result + ((boxesByMasterID == null) ? 0 : boxesByMasterID.hashCode());
		return result;
	}

	public void clean() {
		boxesByCol = null;
		boxesByID = null;
		boxesByPos = null;
		agentsByPos = null;
	}

}
