package com.client.problem.heuristics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map.Entry;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.objects.Position;
import com.client.objects.StateObject;
import com.client.problem.BigActionType;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.client.problem.goal.SingleGoalProblem;
import com.client.problem.goal.action.SingleGoalAction;
import com.search.IProblemAction;
import com.search.Search;

public class NearestFreeHeuristics {
	//calculates heuristics for pushing boxes away from the path
	public static int calculate(WorldState sgState, LinkedHashMap<StateObject, Position> objFreePlace, Agent agent, Box box, LinkedHashSet<Position> goToPath) {
		int objToFreeDistance = 0;
		boolean first = true;//indicates whether this is next box to push away
		Entry<StateObject, Position> lastEntry = null;
		for(Entry<StateObject, Position> e : objFreePlace.entrySet()) {
			StateObject o = e.getKey();
			Position targetPos = e.getValue();
			Position currentBPos = o instanceof Box ? sgState.getBox((Box)o).pos : sgState.getAgent((Agent)o).pos;
			if(targetPos.equals(currentBPos))
				continue;
			if(first) {//adding distance agent-box only to first box on way that wasn't yet moved away
				first = false;
				objToFreeDistance+= Rigids.distance(sgState.getAgent(agent).pos, currentBPos) - 1;
			}
			objToFreeDistance += Rigids.distance(currentBPos, targetPos);
			//now if previous box is not yet completed then add distance between its boxFreePlace and the current box
			if(lastEntry != null) {
				objToFreeDistance += Rigids.distance(lastEntry.getValue(), currentBPos);
			}
			lastEntry = e;
		}
		Box lastBoxOnWay =  getLastConflictingBoxOnPath(sgState, goToPath, objFreePlace);
		//please do not delete it until the final hand-in - I'll test it how it behaves when htn is narrowed down to MST
		//I should also consider changing heuristics for going to goal and checking whether there are boxes conflicting on path to goal
//		if(lastBoxOnWay != null)//using this gives sometimes better solution in hanoi, but slightly makes performance worse in large levels
//			objToFreeDistance += Rigids.distance(objFreePlace.get(lastBoxOnWay), sgState.getBox(box).pos);//from last box's on way target to target box of goTo
//		else 
			objToFreeDistance += Rigids.distance(sgState.getAgent(agent).pos, sgState.getBox(box).pos) - 1;
		return objToFreeDistance;
	}
	
	public static int calculate(WorldState sgState, LinkedHashMap<StateObject, Position> objFreePlace, Agent agent) {
		int objToFreeDistance = 0;
		Entry<StateObject, Position> lastEntry = null;
		for(Entry<StateObject, Position> e : objFreePlace.entrySet()) {
			StateObject o = e.getKey();
			Position targetPos = e.getValue();
			Position currentBPos = o instanceof Box ? sgState.getBox((Box)o).pos : sgState.getAgent((Agent)o).pos;
			//not admissable -> agent may travel with box towards better heuristics and we add distance from agent to its free place
			objToFreeDistance+=Rigids.distance(currentBPos, targetPos);
		}
		return objToFreeDistance;
	}

	public static LinkedHashMap<StateObject, Position> establishHeuristics(WorldState initial, BigActionType type, Agent agent, Box box, Goal goal, LinkedHashSet<Position> goToPath) {
		LinkedList<StateObject> boxesOnWay = new LinkedList<StateObject>();
		LinkedHashMap<StateObject, Position> boxFreePlace = new LinkedHashMap<StateObject, Position>();
		ArrayList<Box> higherPriorityBoxes = Rigids.getBoxesOnGoalsAboveLevel(initial, Rigids.getPriority(goal));

		
		
		LinkedHashSet<Position> path = new LinkedHashSet<Position>();
		SingleGoalProblem carryProblem = new SingleGoalProblem(initial.getRelaxed(agent, box, higherPriorityBoxes), BigActionType.CARRYTO_GOAL, agent, box, goal);
		carryProblem.isRelaxed = true;
		LinkedList<IProblemAction> plan =  Search.aStar(carryProblem);
		Collections.reverse(plan);
		path.add(initial.getAgent(agent).pos);
		if (box != null) path.add(initial.getBox(box).pos);

		if (plan != null) {
			for (IProblemAction a : plan) {
				SingleGoalAction sga = (SingleGoalAction) a;
				if(path.contains(sga.pos))
					path.remove(sga.pos);//ensures that the path is in reverse order
				path.add(sga.pos);
			}
		}

		for (Position p : path) {
			Box b = initial.getBox(p);
			if (b != null && (box == null || b.masterID != box.masterID)) {//&& !boxesOnWay.contains(b)
				boxesOnWay.add(b);
			}
		}

		Box b;
		while(!boxesOnWay.isEmpty()) {
			b = (Box) boxesOnWay.remove();
			NearestFreeProblem problem = new NearestFreeProblem(initial, path, b, boxFreePlace, boxesOnWay, box, goToPath);
			//boxFreePlace and boxesOnWay are modified during search DO NOT REFACTOR IT!!!!!
			Search.breadthFirstSearch(problem);
		}
		//reversing LinkedHashMap of boxFreePlace. This has to be done for heuristics calculation!
		LinkedHashMap<StateObject, Position> boxFreePlaceReversed = new LinkedHashMap<StateObject, Position>();
		ArrayList<Entry<StateObject, Position>> entries = new ArrayList<Entry<StateObject, Position>>(boxFreePlace.entrySet());
		ListIterator<Entry<StateObject, Position>> it = entries.listIterator(entries.size());
		while(it.hasPrevious()) {
			Entry<StateObject, Position> e = it.previous();
			boxFreePlaceReversed.put(e.getKey(), e.getValue());
		}
		return boxFreePlaceReversed;
	}

	public static LinkedHashSet<Position> getGoToPath(WorldState initial,
			Agent agent, Box box, Goal goal, ArrayList<Box> higherPriorityBoxes) {
		SingleGoalProblem goToProblem = new SingleGoalProblem(initial.getRelaxed(agent, box, higherPriorityBoxes), BigActionType.GOTO_BOX, agent, box, goal);
		LinkedList<IProblemAction> goToPlan =  Search.aStar(goToProblem);
		LinkedHashSet<Position> goToPath = new LinkedHashSet<Position>();
		goToPath.add(initial.getAgent(agent).pos);
		if (box != null) goToPath.add(initial.getBox(box).pos);

		if (goToPlan != null) {
			for (IProblemAction a : goToPlan) {
				SingleGoalAction sga = (SingleGoalAction) a;
				if(goToPath.contains(sga.pos))
					goToPath.remove(sga.pos);//ensures that the path is in reverse order
				goToPath.add(sga.pos);
			}
		}
		return goToPath;
	}
	
	public static boolean isPathFree(WorldState sgState, LinkedHashSet<Position> path, LinkedHashMap<StateObject, Position> objFreePlace) {
		if(path == null || path.isEmpty())
			return true;
		boolean isFree = true;
		for(Entry<StateObject, Position> e : objFreePlace.entrySet()) {
			StateObject o = e.getKey(); 
			Position currentBPos = o instanceof Box ? sgState.getBox((Box)o).pos : sgState.getAgent((Agent)o).pos;
			if(path.contains(currentBPos))
				isFree = false;
		}
		return isFree;
	}
	
	public static Box getLastConflictingBoxOnPath(WorldState sgState, LinkedHashSet<Position> goToPath, LinkedHashMap<StateObject, Position> objFreePlace) {
		if(goToPath == null)
			return null;
		StateObject box = null;
		for(Entry<StateObject, Position> e : objFreePlace.entrySet()) {
			StateObject o = e.getKey(); 
			Position currentBPos = o instanceof Box ? sgState.getBox((Box)o).pos : sgState.getAgent((Agent)o).pos;
			if(goToPath.contains(currentBPos))
				box = o;
		}
		return (Box)box;
	}
	
}
