package com.client.problem.heuristics;

import java.util.Collections;
import java.util.LinkedList;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.objects.Position;
import com.client.objects.StateObject;
import com.client.problem.BigActionType;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.search.AbstractSearchProblem;
import com.search.IProblemAction;
import com.search.IProblemState;
import com.search.SearchNode;

public class SkipBoxProblem extends AbstractSearchProblem {
	
	private WorldState initial;
	private Agent currentAgent;
	private Box currentBox;
	private Goal goal;
	private BigActionType type;
	private StateObject obstacle;
	private Position agntPos;
	private Position boxPos;
	private int initialDistance;

	
	
	
	public class SkipBoxAction implements IProblemAction{
		public Agent agent;
		public Box box;
		public int stepCost=1;
		private int limit=4;
	
		public SkipBoxAction(Agent agent, Box box) {
			this.agent=agent;
			this.box=box;
			
		}
	}
	
	private class ObjPositionState implements IProblemState{

		public Agent agent;
		public Box box;
		public boolean cutOff=false;
		private int limit=4;

		private static final int prime = 0;
		private static final int base = 0;
		
		
		public ObjPositionState(Agent agent, Box box) {
			this.agent=agent;
			this.box=box;
			
		}
		
		@Override
		public boolean equals(Object o) {
			if (o != null && o.getClass().equals(getClass()))
			{
				ObjPositionState temp = (ObjPositionState) o;
				return agent.equals(temp.agent)
						&& box.equals(box);
				
			}
			else return false;
		}
		
		@Override
		public int hashCode() {
			int result = base;
			result = prime * result + ((agent == null) ? 0 : agent.hashCode())+
					((box ==null)?0:box.hashCode());
			return result;
		}

		@Override
		public long longHashCode() {
			return hashCode();
		}
	}
	
	public SkipBoxProblem(WorldState initial, Agent currentAgent, Box currentBox, Goal goal, BigActionType type, StateObject obstacle)
	{
		this.initial = initial;
		this.currentAgent=initial.getAgent(currentAgent);
		this.currentBox=initial.getBox(currentBox);
		this.goal=goal;
		this.type=type;
		this.obstacle=initial.getStateObject(obstacle);
		this.agntPos=initial.getAgent(currentAgent).pos;
	    this.boxPos=initial.getBox(currentBox).pos;
		if(type.equals(BigActionType.GOTO_BOX))
		{
			this.initialDistance=Rigids.distance(currentAgent.pos, currentBox.pos);
		}
		else
		{
			this.initialDistance=Rigids.distance(currentAgent.pos, currentBox.pos)+Rigids.distance(currentBox.pos, goal.pos);
		}
	}
	
	public SkipBoxProblem(WorldState initial, Agent currentAgent, Box currentBox, BigActionType type, StateObject obstacle)
	{
		this(initial, currentAgent, currentBox, null, type, obstacle);
	}

	@Override
	public boolean goalTest(SearchNode node) {
		ObjPositionState bpState = (ObjPositionState) node.state;
		int distance;
		if(type.equals(BigActionType.GOTO_BOX))
		{
			distance=Rigids.distance(bpState.agent.pos, currentBox.pos);
		}
		else
		{
			distance=Rigids.distance(bpState.agent.pos, bpState.box.pos)+Rigids.distance(bpState.box.pos, goal.pos);
		}
		
		return (distance+2)<=this.initialDistance;
	}
	
//	@Override
//	public LinkedList<IProblemAction> buildSolution(SearchNode node) {
//		LinkedList<IProblemAction> actions = new LinkedList<IProblemAction>();
//		actions.add(node.action);
//		Collections.reverse(actions);
//		return actions;
//	}
	
	@Override
	public SearchNode initialState() {
		SearchNode nsn = new SearchNode();
		nsn.parent = null;
		nsn.state = new ObjPositionState(currentAgent, currentBox);
		nsn.action = null;
		return nsn;
	}

	@Override
	public LinkedList<IProblemAction> getActions(SearchNode node) {
		LinkedList<IProblemAction> result = new LinkedList<IProblemAction>();
		ObjPositionState state = (ObjPositionState) node.state;
		StateObject newObstacle;
		if(state.cutOff)
		{
			return result;
		}
		boolean isDifferentColorObject=false;
		for(Position p : Rigids.nearbyPositions(state.agent.pos).values()) {
			newObstacle=initial.getStateObject(p);
			isDifferentColorObject=newObstacle!=null && !newObstacle.color.equals(currentAgent.color) && !obstacle.equals(newObstacle);
			if(initial.isFree(p) || isDifferentColorObject || (currentBox!=null && p.equals(boxPos))) {
				if(type.equals(BigActionType.GOTO_BOX) || !state.box.pos.equals(p))
				{
					Box stateBox=state.box;
					Agent stateAgnt = new Agent(state.agent);
					Position bpos=stateAgnt.pos;
					stateAgnt.pos=p;
					if(type.equals(BigActionType.CARRYTO_GOAL))
					{
					  stateBox=new Box(state.box);
				      stateBox.pos=bpos;
					}
				    SkipBoxAction sba=new SkipBoxAction(stateAgnt, stateBox);
				    if(isDifferentColorObject)
				    {
				    	sba.stepCost++;
					    sba.limit++;
				    }
				    result.add(sba);
				}
			}	
		}
		
		if(type.equals(BigActionType.CARRYTO_GOAL))
		{
			
			for(Position p : Rigids.nearbyPositions(state.box.pos).values())
			{
				newObstacle=initial.getStateObject(p);
				isDifferentColorObject=newObstacle!=null && !newObstacle.color.equals(currentAgent.color) && !obstacle.equals(newObstacle);
				if(initial.isFree(p) || isDifferentColorObject || p.equals(agntPos))
				{
					if(!state.agent.pos.equals(p))
					{
						Box stateBox=new Box(state.box);
						Position apos=stateBox.pos;
						Agent stateAgnt = new Agent(state.agent);
						stateAgnt.pos=apos;
						stateBox.pos=p;
					    SkipBoxAction sba=new SkipBoxAction(stateAgnt, stateBox);
					    if(isDifferentColorObject)
					    {
					    	sba.stepCost++;
						    sba.limit++;
					    }
					    result.add(sba);
					}
				}
			}
		}
		return result;
	}

	@Override
	public SearchNode childNode(SearchNode node, IProblemAction action) {
		SkipBoxAction sbAction = (SkipBoxAction) action;
		SearchNode nsn = new SearchNode();
		nsn.parent = node;
		ObjPositionState opState=new ObjPositionState(sbAction.agent, sbAction.box);
		nsn.state = opState;
		nsn.action = sbAction;
		nsn.pathCost=node.pathCost+sbAction.stepCost;
		if(this.type.equals(BigActionType.GOTO_BOX))
		{
			nsn.heuristicCost=Rigids.distance(sbAction.agent.pos, currentBox.pos);
		}
		else
		{
			nsn.heuristicCost=Rigids.distance(sbAction.agent.pos, sbAction.box.pos)+Rigids.distance(sbAction.box.pos, goal.pos);
		}
		//int distanceBox=((currentBox!=null)?Rigids.distance(currentBox.pos, sbAction.box.pos):0);
		//int distanceAgent=Rigids.distance(currentAgent.pos, sbAction.agent.pos);
		//nsn.heuristicCost=((currentBox!=null)?Rigids.distance(sbAction.agent.pos, sbAction.box.pos):0)+ ((distanceAgent < distanceBox)?distanceBox:distanceAgent);
		if(nsn.pathCost > sbAction.limit)
		{
			opState.cutOff=true;
		}
		return nsn;
	}

	@Override
	public String toString() {
		return "SkipBoxProblem {"+ currentAgent + ", "+ currentBox + ", " + initial + ", " + "}";
	}
	

}
