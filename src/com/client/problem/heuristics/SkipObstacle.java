package com.client.problem.heuristics;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.objects.Position;
import com.client.objects.StateObject;
import com.client.problem.BigActionType;
import com.client.problem.WorldState;
import com.client.problem.heuristics.SkipBoxProblem.SkipBoxAction;
import com.search.IProblemAction;
import com.search.Search;

public class SkipObstacle {
	
	public static boolean canSkip(WorldState initial, Agent currentAgent, Box currentBox, Goal goal, BigActionType type, StateObject obstacle, boolean skipMany)
	{
		Agent previousAgent;
		Box previousBox;
		SkipBoxProblem problem;
		LinkedList<IProblemAction> actions;
		while(obstacle!=null && !obstacle.color.equals(currentAgent.color))
		{
			problem= new SkipBoxProblem(initial, currentAgent, currentBox, goal, type, obstacle);
			actions=Search.aStar(problem);
			if(actions==null)
			{
				return false;
			}
			
			previousAgent=currentAgent;
			previousBox=currentBox;
			for(IProblemAction act : actions)
			{
				SkipBoxAction sba= (SkipBoxAction) act;
				obstacle=initial.getStateObject(sba.agent.pos);
				if(obstacle!=null && !obstacle.color.equals(currentAgent.color))
				{
					currentAgent=previousAgent;
					currentBox=previousBox;
					if(!skipMany)
					{
						return false;
					}
					break;
				}
				if(type.equals(BigActionType.CARRYTO_GOAL))
				{
					obstacle=initial.getStateObject(sba.box.pos);
					if(obstacle!=null && !obstacle.color.equals(currentAgent.color))
					{
						currentAgent=previousAgent;
						currentBox=previousBox;
						if(!skipMany)
						{
							return false;
						}
						break;
					}
				}
	
				previousAgent=sba.agent;
				previousBox=sba.box;
				
			}
		}
		
		return true;
		
	}

}
