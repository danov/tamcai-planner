package com.client.problem.heuristics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import com.client.objects.Box;
import com.client.objects.Position;
import com.client.objects.StateObject;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.search.AbstractSearchProblem;
import com.search.IProblemAction;
import com.search.IProblemState;
import com.search.SearchNode;

public class NearestFreeProblem extends AbstractSearchProblem {

	private StateObject obj;
	private WorldState initial;
	private HashSet<Position> occupied;
	private HashMap<StateObject, Position> objFreePlace;
	private LinkedList<StateObject> objOnWay;
	private boolean isObjOnGoToPath = false;
	private Box targetBox;
	
	public class NearestFreeAction implements IProblemAction{
		public StateObject obj;
		
		public NearestFreeAction(StateObject obj) {
			this.obj = obj;
		}
	}
	
	private class ObjPositionState implements IProblemState{

		public StateObject obj;

		private static final int prime = 0;
		private static final int base = 0;
		
		
		public ObjPositionState(StateObject obj) {
			this.obj = obj;
		}
		
		@Override
		public boolean equals(Object o) {
			ObjPositionState temp = (ObjPositionState) o;
			if (o != null && o.getClass().equals(getClass()))
				return obj.equals(temp.obj);
			else return false;
		}
		
		@Override
		public int hashCode() {
			int result = base;
			result = prime * result + ((obj == null) ? 0 : obj.hashCode());
			return result;
		}

		@Override
		public long longHashCode() {
			return hashCode();
		}
	}
	
	public NearestFreeProblem(WorldState initial, HashSet<Position> occupied, StateObject obj, HashMap<StateObject, Position> objFreePlace, LinkedList<StateObject> objOnWay, Box targetBox, LinkedHashSet<Position> goToPath) {
		this.occupied = occupied;
		this.initial = initial;
		this.obj = (StateObject) obj.clone();
		this.objFreePlace = objFreePlace;
		this.objOnWay = objOnWay;
		if(goToPath != null)
			this.isObjOnGoToPath = goToPath.contains(obj.pos);
		this.targetBox = targetBox;
	}

	@Override
	public boolean goalTest(SearchNode node) {
		ObjPositionState bpState = (ObjPositionState) node.state;
		if(!occupied.contains(bpState.obj.pos)) {
			//if nearest free place is in immediate neighborhood then discard that box as conflicting box
			if(Rigids.distance(obj.pos, bpState.obj.pos) == 1) 
				return true;
			occupied.add(bpState.obj.pos);
			objFreePlace.put(obj, bpState.obj.pos);
			if(!initial.isFree(bpState.obj.pos) && initial.getBox(bpState.obj.pos) != null) {
				//here adding to boxesOnWay the box that is in place where we can put our box from initial problem
				objOnWay.addLast(initial.getBox(bpState.obj.pos));
			}
			return true;
		}
		else return false;
	}
	
	@Override
	public LinkedList<IProblemAction> buildSolution(SearchNode node) {
		LinkedList<IProblemAction> actions = new LinkedList<IProblemAction>();
		actions.add(node.action);
		return actions;
	}
	
	@Override
	public SearchNode initialState() {
		SearchNode nsn = new SearchNode();
		nsn.parent = null;
		nsn.state = new ObjPositionState(obj);
		nsn.action = null;
		return nsn;
	}

	@Override
	public LinkedList<IProblemAction> getActions(SearchNode node) {
		LinkedList<IProblemAction> result = new LinkedList<IProblemAction>();
		ObjPositionState state = (ObjPositionState) node.state;
		for(Position p : Rigids.nearbyPositions(state.obj.pos).values()) {
			if(!Rigids.walls[p.x][p.y] && (initial.getBox(state.obj.pos) == null || !initial.getBox(state.obj.pos).equals(targetBox) || !isObjOnGoToPath) ) {
				StateObject o = (StateObject) obj.clone();
				o.pos = p;
				result.add(new NearestFreeAction(o));
			}
		}
		return result;
	}

	@Override
	public SearchNode childNode(SearchNode node, IProblemAction action) {
		NearestFreeAction nfAction = (NearestFreeAction) action;
		SearchNode nsn = new SearchNode();
		nsn.parent = node;
		nsn.state = new ObjPositionState(nfAction.obj);
		nsn.action = nfAction;		
		return nsn;
	}

	@Override
	public String toString() {
		return "NearestFreeProblem {"+ obj + ", " + initial + ", " + "}";
	}
	

}
