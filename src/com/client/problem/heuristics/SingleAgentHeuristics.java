package com.client.problem.heuristics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.objects.StateObject;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.client.util.PathEdge;
import com.client.util.UnionFind;
import com.search.util.SimpleFastPriorityQueue;

public class SingleAgentHeuristics {

	public static long millisec = 0;

	public static int calculate(WorldState state, Agent a) {
		int maxVal = 0;
		millisec = System.currentTimeMillis();
		LinkedList<PathEdge> linked = approximatedShortestPath(state, a);
		for(PathEdge pe : linked) {
			maxVal += pe.distance();
		}
		millisec = System.currentTimeMillis() - millisec;
		return maxVal > 0 ? maxVal : 0;
	}
	
	public static LinkedList<PathEdge> approximatedShortestPath(WorldState state, Agent a) {
		Agent agent = state.getAgent(a);

		SimpleFastPriorityQueue<PathEdge> queue = new SimpleFastPriorityQueue<PathEdge>();
		UnionFind<StateObject> sets = new UnionFind<StateObject>();
		ArrayList<PathEdge> MST = new ArrayList<PathEdge>();

		HashMap<Goal, PathEdge> inGoal = new HashMap<Goal, PathEdge>();
		HashMap<Goal, PathEdge> outGoal = new HashMap<Goal, PathEdge>();

		HashMap<Box, PathEdge> inBox = new HashMap<Box, PathEdge>();
		HashMap<Box, PathEdge> outBox = new HashMap<Box, PathEdge>();

		PathEdge agentEdge = null;
		
		sets.makeSet(agent);
		
		for (Goal g : Rigids.getPriorities().keySet()) {
			if(g.color.equals(agent.color)) {
				if (!state.isAchieved(g)) {
					sets.makeSet(g);
					for (Box b : state.getBoxes(g.color)) {
						if (!state.isOnGoal(b)) {
							sets.makeSet(b);
							// PathEdges goal/box
							if (g.id.equals(b.id)) {
								queue.offer(new PathEdge(b, g));
							} else {
								queue.offer(new PathEdge(g, b));
							}
						}
					}
				}
			}
		}

		// Kruskal's algorithm without the not needed boxes
		while (queue.size() > 0) {
			PathEdge e = queue.poll();
			if (!sets.sameSet(e.node1, e.node2)) {
				PathEdge pe = e;

				if (e.node1.getClass().equals(Goal.class) || e.node2.getClass().equals(Goal.class)) {
					boolean inBound = e.node2.getClass().equals(Goal.class);
					Goal goal = e.node1.getClass().equals(Goal.class) ? (Goal) e.node1 : (Goal) e.node2;
					Box box = e.node1.getClass().equals(Box.class) ? (Box) e.node1 : (Box) e.node2;

					if (inGoal.containsKey(goal) && outGoal.containsKey(goal)) continue;
					if (inBox.containsKey(box) && outBox.containsKey(box)) continue;

					if (!inGoal.containsKey(goal) && !outBox.containsKey(box) && inBound) {
						// Add box-goal connection
						inGoal.put(goal, pe);
						outBox.put(box, pe);
					} else if (!outGoal.containsKey(goal) && !inBox.containsKey(box)) {
						// Add goal-box connection
						if (inBound) {
							pe = new PathEdge(e.node2, e.node1);
						}

						outGoal.put(goal, pe);
						inBox.put(box, pe);
					} else continue;

				}

				sets.union(pe.node1, pe.node2);
				MST.add(pe);
			} else {
				// If box to goal not yet added, add it
				if (e.node2.getClass().equals(Goal.class)) {
					Goal goal = (Goal) e.node2;
					Box box = (Box) e.node1;

					if (inGoal.containsKey(goal) && outGoal.containsKey(goal)) continue;
					if (inBox.containsKey(box) && outBox.containsKey(box)) continue;
					if (inGoal.containsKey(goal) || outBox.containsKey(box)) continue;

					inGoal.put(goal, e);
					outBox.put(box, e);
					// Break outgoing connection to prevent loops
					if(inBox.containsKey(box) && outGoal.containsKey(goal)) {
						if (inBox.get(box).distance() >= outGoal.get(goal).distance()) {
							MST.remove(inBox.get(box));
							outGoal.remove(inBox.get(box).node1);
							inBox.remove(box);
						} else {
							MST.remove(outGoal.get(goal));
							inBox.remove(outGoal.get(goal).node2);
							outGoal.remove(goal);
						}
					}

					MST.add(e);
				}
			}
		}

		// Consider goal priorities

		// Find all odd degree boxes and goals and connect them accordingly
		// Remove all goal-box edges, where the boxes does not have corresponding goal
		Iterator<Goal> it = outGoal.keySet().iterator();
		while (it.hasNext()) {
			Goal g = it.next();
			if (!outBox.containsKey(outGoal.get(g).node2)) {
				MST.remove(outGoal.get(g));
				inBox.remove(outGoal.get(g).node2);
				it.remove();
			}
		}

		// Build the new queue 
		queue = new SimpleFastPriorityQueue<PathEdge>();
		for (Box b : outBox.keySet()) {
			if (!inBox.containsKey(b)) {
				PathEdge pe;
				for (Goal g : inGoal.keySet()) {
					if (!outGoal.containsKey(g) && !inGoal.get(g).node1.equals(b)) {
						pe = new PathEdge(g, b);
						queue.add(pe);
					}
				}
			}
		}

		// Connect the odd-degree goals to the odd-degree boxes and the agent remove the agent and add it later
		while (queue.size() > 0) {
			PathEdge e = queue.poll();
			if (!sets.sameSet(e.node1, e.node2) && !inBox.containsKey(e.node2)) {
				outGoal.put((Goal) e.node1, e);
				inBox.put((Box) e.node2, e);

				sets.union(e.node1, e.node2);
				MST.add(e);
			}
		}
		
		// Create the linked list
		LinkedList<PathEdge> linked = new LinkedList<PathEdge>();
		while(linked.size() != MST.size()) {
			for(PathEdge pe : MST) {
				if(linked.contains(pe)) continue;
				if(linked.isEmpty()) {
					if(pe.node1 instanceof Box && !inBox.containsKey(pe.node1)) {
						linked.add(pe);
						break;
					} else continue;
				}
				
				if(linked.getLast().node2.equals(pe.node1)) {
					linked.add(pe);
					break;
				}
			}
		}
		
		// Reorder according to priorities
		Map<Goal, Integer> priorities = Rigids.getPriorities();
		for(Goal g : priorities.keySet()) {
			if(g.color.equals(agent.color) && priorities.get(g) > 0) {
				if(inGoal.containsKey(g) && inBox.containsKey(inGoal.get(g).node1)) {
					Box box = (Box) inGoal.get(g).node1;
					Goal goal = (Goal) inGoal.get(g).node2;
					Goal prevGoal = (Goal) inBox.get(box).node1;

					// Remove the edge head
					inBox.remove(box);
					outGoal.remove(prevGoal);
					linked.remove(new PathEdge(prevGoal, box));
					
					// If edge tail exists, remove it and shortcut it
					if(outGoal.containsKey(goal)) {
						Box nextBox = (Box) outGoal.get(goal).node2;
						linked.remove(new PathEdge(goal, nextBox));
						outGoal.remove(goal);
						inBox.remove(nextBox);
						
						PathEdge pe = new PathEdge(prevGoal, nextBox);
						outGoal.put(prevGoal, pe);
						inBox.put(nextBox, pe);
						int ind = linked.indexOf(inGoal.get(prevGoal));
						linked.add(ind + 1, pe);
					}
					
					linked.remove(new PathEdge(box, goal));
	
					PathEdge newTail = new PathEdge(goal, linked.getFirst().node1);
					inBox.put((Box) newTail.node2, newTail);
					outGoal.put(goal, newTail);
					linked.add(0, newTail);
					linked.add(0, new PathEdge(box, goal));
				}
			}
		}
		
		// Add the agent
		if(!linked.isEmpty()) {
			agentEdge = new PathEdge(agent, linked.getFirst().node1);
			linked.add(0, agentEdge);
		}
		
		return linked;
	}
}
