package com.client;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.client.objects.Agent;
import com.client.objects.Box;
import com.client.objects.Goal;
import com.client.problem.Rigids;
import com.client.problem.WorldState;
import com.client.problem.goal.action.SingleGoalAction;
import com.client.problem.multi.MultiAgentPlaner;
import com.client.problem.priority.GoalPriorities;
import com.client.util.DistanceCalculator;
import com.search.IProblemAction;

public class TAMCAIClient {

	private static BufferedReader in;

	private WorldState initial;
	private ArrayList<LinkedList<IProblemAction>> plans = new ArrayList<LinkedList<IProblemAction>>();

	public TAMCAIClient() throws IOException {
		readMap();
		
		MultiAgentPlaner.initialize(initial);
		MultiAgentPlaner.plan();
		plans = Rigids.getOrderedPlans();
		System.err.println("Plan steps: " + plans.get(0).size());
		System.err.println(plans.get(0));
	}

	private void readMap() throws IOException {
		Map<Character, String> colors = new HashMap<Character, String>();
		ArrayList<Agent> agents = new ArrayList<Agent>();
		ArrayList<Box> boxes = new ArrayList<Box>();

		ArrayList<String> readLines = new ArrayList<String>();
		int maxLineLength = 0;
		String line, color;

		// Read lines specifying colors
		while ((line = in.readLine()).matches("^[a-z]+:\\s*[0-9A-Z](,\\s*[0-9A-Z])*\\s*$")) {
			line = line.replaceAll("\\s", "");
			color = line.split(":")[0];

			for (String id : line.split(":")[1].split(","))
				colors.put(id.charAt(0), color);
		}

		// Read lines specifying level layout
		// Just calculate sizes of the level
		int height = 0;
		while (line != null && !line.equals("")) {
			readLines.add(line);
			if (line.length() > maxLineLength) maxLineLength = line.length();
			line = in.readLine();
			height++;
		}

		// Initialize arrays for places
		Rigids.walls = new boolean[maxLineLength][height];
		int masterID = 467;
		for (int y = 0; y < readLines.size(); y++) {
			for (int x = 0; x < maxLineLength; x++) {
				if (readLines.get(y).length() > x) {
					String row = readLines.get(y);
					Character id = row.charAt(x);

					if ('0' <= id && id <= '9') {
						if (colors.get(id) != null) agents.add(new Agent(id, colors.get(id), x, y));
						else agents.add(new Agent(id, "magic", x, y));
					}

					if ('a' <= id && 'z' >= id) {
						String goalColor = colors.get(Character.toUpperCase(id)) == null ? "magic" : colors.get(Character.toUpperCase(id));
						Goal g = new Goal(masterID, id, goalColor, x, y);
						masterID = masterID * 127;
						Rigids.addGoal(g);
					}

					if ('A' <= id && 'Z' >= id) {
						if (colors.get(id) != null) boxes.add(new Box(masterID, Character.toLowerCase(id), colors.get(id), x, y));
						else boxes.add(new Box(masterID, Character.toLowerCase(id), "magic", x, y));
						masterID++;
					}

					if (' ' == id) {

					}

					if ('+' != id) {
						Rigids.walls[x][y] = false;
					} else {
						Rigids.walls[x][y] = true;
					}

				} else {
					Rigids.walls[x][y] = true;
				}
			}
		}

		Rigids.distances = DistanceCalculator.calculateDistances(Rigids.walls, agents.get(0).pos.x, agents.get(0).pos.y);
		initial = new WorldState(agents, boxes);
		Rigids.setGoalPriorities(GoalPriorities.getGoalsPriorities(initial));
	}

	public boolean update() throws IOException {
		SingleGoalAction sga = null;
		String traceAction = "[";
		String jointAction = "[";

		for (int i = 0; i < plans.size() - 1; i++) {
			if (plans.size() > 0 && plans.get(i) != null && plans.get(i).size() > 0) {
				sga = (SingleGoalAction) plans.get(i).removeFirst();
				jointAction += sga + ",";
				traceAction += sga + ": " + sga.heuristicCost + ",";
			}
		}

		if (plans.size() > 0 && plans.get(plans.size() - 1) != null && plans.get(plans.size() - 1).size() > 0) {
			sga = (SingleGoalAction) plans.get(plans.size() - 1).removeFirst();
			jointAction += sga + "]";
			traceAction += sga + ": " + sga.heuristicCost + "]";
			System.err.println(traceAction);
			System.out.println(jointAction);
		}

		System.out.flush();
		// Disregard these for now, but read or the server stalls when
		// outputbuffer gets filled!
		String percepts = in.readLine();
		if (percepts == null) return false;

		return true;
	}

	public static void main(String[] args) {
		// Use stderr to print to console
		System.err.println("Hello from TAMCAIClient. I am sending this using the error outputstream.");
		try {
			if (args.length > 0 && args[0].equals("debug")) {
				in = new BufferedReader(new FileReader("levels/ivan.lvl"));
			} else {
				in = new BufferedReader(new InputStreamReader(System.in));
			}

			TAMCAIClient client = new TAMCAIClient();
			while (client.update());

		} catch (IOException e) {
			// Got nowhere to write to probably
		}
	}
}
