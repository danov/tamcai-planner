package com.search;

import java.util.Collections;
import java.util.LinkedList;

public abstract class AbstractSearchProblem {

	public String searchTrace = "";

	public boolean trace = false;
	public long generatedNodes = 0;
	public long exploredStates = 0;
	public long frontierMaximum = 0;
	public boolean cutOffFlag = false;

	public abstract boolean goalTest(SearchNode node);

	public abstract SearchNode initialState();

	public abstract LinkedList<IProblemAction> getActions(SearchNode node);

	public abstract SearchNode childNode(SearchNode node, IProblemAction action);

	public LinkedList<IProblemAction> buildSolution(SearchNode node) {
		if (cutOffFlag) return null;
		LinkedList<IProblemAction> actions = new LinkedList<IProblemAction>();
		while (node != null) {
			if (node.action != null) actions.add(node.action);
			node = node.parent;
		}

		Collections.reverse(actions);
		return actions;
	}
}
