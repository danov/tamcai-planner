package com.search.util;

import java.util.Comparator;

import com.search.SearchNode;

public class AStarComparator implements Comparator<SearchNode> {

	@Override
	public int compare(SearchNode a, SearchNode b) {
		int diff = ((a.pathCost + a.heuristicCost) - (b.pathCost + b.heuristicCost));
		return diff == 0 ? b.pathCost - a.pathCost : diff;
	}

	private static AStarComparator instance = null;

	public static AStarComparator getInstance() {
		if (instance == null) instance = new AStarComparator();
		return instance;
	}
}
