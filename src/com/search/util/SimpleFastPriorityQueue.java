package com.search.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Queue;

import org.teneighty.heap.FibonacciHeap;
import org.teneighty.heap.Heap.Entry;

/**
 * Expects elements with non-colliding hashCodes. If you have collisions, use interface ILongHashCode
 * 
 * @author Ivan
 * 
 * @param <T>
 */
public class SimpleFastPriorityQueue<T> implements Queue<T> {

	HashMap<Long, ArrayList<Entry<T, Long>>> keymap;
	FibonacciHeap<T, Long> queue;
	Comparator<T> comparator;

	private long curID = 0;

	public SimpleFastPriorityQueue() {
		keymap = new HashMap<Long, ArrayList<Entry<T, Long>>>();
		queue = new FibonacciHeap<T, Long>();
	}

	public SimpleFastPriorityQueue(Comparator<T> comparator) {
		keymap = new HashMap<Long, ArrayList<Entry<T, Long>>>();
		queue = new FibonacciHeap<T, Long>(comparator);
		this.comparator = comparator;
	}

	@Override
	public int size() {
		return queue.getSize();
	}

	@Override
	public boolean isEmpty() {
		return queue.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		if (getEntry(o) == null) return false;
		else return true;
	}

	private Entry<T, Long> getEntry(Object o) {
		Long hash = o instanceof ILongHashCode ? ((ILongHashCode) o).longHashCode() : (long) o.hashCode();
		Entry<T, Long> entry = null;
		if (keymap.containsKey(hash)) {
			for (Entry<T, Long> el : keymap.get(hash)) {
				if (el.getKey().equals(o)) {
					entry = el;
					break;
				}
			}
		}
		return entry;
	}

	@Override
	public Iterator<T> iterator() {
		return queue.getKeys().iterator();
	}

	@Override
	public Object[] toArray() {
		return queue.getKeys().toArray();
	}

	@Override
	public <TT> TT[] toArray(TT[] a) {
		return queue.getKeys().toArray(a);
	}

	@Override
	public boolean remove(Object o) {
		try {
			Long hash = o instanceof ILongHashCode ? ((ILongHashCode) o).longHashCode() : (long) o.hashCode();
			Entry<T, Long> entry = getEntry(o);
			keymap.get(hash).remove(entry);
			if (keymap.get(hash).size() == 0) keymap.remove(hash);
			queue.delete(entry);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		curID = 0;
		keymap.clear();
		queue.clear();
	}

	@Override
	public boolean add(T o) {
		return offer(o);
	}

	@Override
	public boolean offer(T o) {
		try {
			Long hash = o instanceof ILongHashCode ? ((ILongHashCode) o).longHashCode() : (long) o.hashCode();
			curID++;
			Entry<T, Long> entry = queue.insert(o, curID);
			if (!keymap.containsKey(hash)) keymap.put(hash, new ArrayList<Entry<T, Long>>());
			keymap.get(hash).add(entry);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	@Override
	public T remove() {
		return poll();
	}

	@Override
	public T poll() {
		Entry<T, Long> element;
		try {
			element = queue.extractMinimum();
			keymap.remove(element.getValue());
			return element.getKey();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public T element() {
		return peek();
	}

	@Override
	public T peek() {
		Entry<T, Long> element;
		try {
			element = queue.getMinimum();
			return element.getKey();
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public boolean decrease(T o) {
		Entry<T, Long> element = getEntry(o);
		if (!element.getKey().equals(o)) System.err.println("SimpleFastPriorityQueue: Collision " + element.getKey() + " with " + o);
		if ((o instanceof Comparable && ((Comparable<T>) o).compareTo(element.getKey()) < 0)
				|| (comparator != null && comparator.compare(o, element.getKey()) < 0)) {
			try {
				queue.decreaseKey(element, o);
				return true;
			} catch (Exception ex) {
				return false;
			}
		} else return false;
	}

	@SuppressWarnings("unchecked")
	public boolean increase(T o) {
		Entry<T, Long> element = getEntry(o);
		if (!element.getKey().equals(o)) System.err.println("SimpleFastPriorityQueue: Collision " + element.getKey() + " with " + o);
		if ((o instanceof Comparable && ((Comparable<T>) o).compareTo(element.getKey()) > 0)
				|| (comparator != null && comparator.compare(o, element.getKey()) > 0)) {
			try {
				this.remove(o);
				this.add(o);
				return true;
			} catch (Exception ex) {
				return false;
			}
		} else return false;
	}

}
